@extends('backend.layouts.master')
@section('home_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage FAQ</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">FAQ</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
    
        <!-- Main row -->
        <div class="row">

          <!-- Left col -->
          <section class="col-md-12">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3>Edit FAQ
                <a href="{{route('faq.view')}}" class="btn btn-success btn-sm float-right"><i class="fa fa-list"></i> FAQ List</a>
                </h3>
              </div><!-- /.card-header -->
              <div class="card-body">
                 <form action="{{route('faq.update', $editData->id)}}" method="post" id="myForm">
                   @csrf
                   <div class="form-row">
                    
                      
                       <div class="form-group col-md-6">
                         <label for="long_title">Question</label>
                         <textarea class="form-control" name="question" cols="4" rows="4" >{{$editData->question}}</textarea>
                       </div>

                       <div class="form-group col-md-6">
                         <label for="ans">Answer</label>
                         <textarea class="form-control" name="ans" cols="4" rows="4" >{{$editData->ans}}</textarea>
                       </div>
                  

                     <div class="form-group col-md-6">
                       <input type="submit" value="Update" class="btn btn-primary">
                     </div>

                   </div>
                 </form> 
              </div><!-- /.card-body -->
            </div>
          </section>
<!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script>
$(function () {
  $('#myForm').validate({
    rules: {
      question: {
        required: true,
      },

      ans: {
        required: true,
      },

      
    },

    messages: {
      question: {
        required: "Please select user role",
        
      },
      ans: {
        required: "Please enter user name",
        
      },
      
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection