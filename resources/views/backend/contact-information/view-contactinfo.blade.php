@extends('backend.layouts.master')
@section('home_content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Conatct Informaiton</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Contact Info</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
    
        <!-- Main row -->
        <div class="row">

          <!-- Left col -->
          <section class="col-md-12">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3>Update Info
                  <!-- <a href="{{route('logos.view')}}" class="btn btn-success btn-sm float-right"><i class="fa fa-list"></i> View Logos</a> -->
                </h3>
              </div><!-- /.card-header -->
              <div class="card-body">
                 <form action="{{route('contactinfo.update',$data->id)}}" method="post" enctype="multipart/form-data">
                   @csrf
          <div class="form-row">
                       
            <div class="form-group col-md-4">
              <label for="number">Phone Number</label>
              <input type="text" name="number" class="form-control" value="{{$data->number}}">
            </div>

            <div class="form-group col-md-4">
              <label for="email">Email</label>
              <input type="email" name="email" class="form-control" value="{{$data->email}}">
            </div>

            <div class="form-group col-md-4">
              <label for="address">Address</label>
              <input type="text" name="address" class="form-control" value="{{$data->address}}">
            </div>

            <div class="form-group col-md-4">
              <label for="facebook">Facebook</label>
              <input type="text" name="facebook" class="form-control" value="{{$data->facebook}}">
            </div>

            <div class="form-group col-md-4">
              <label for="twitter">Twitter</label>
              <input type="text" name="twitter" class="form-control" value="{{$data->twitter}}">
            </div>

            <div class="form-group col-md-4">
              <label for="linkedin">Linkedin</label>
              <input type="text" name="linkedin" class="form-control" value="{{$data->linkedin}}">
            </div>

            <div class="form-group col-md-4">
              <label for="pinterest">Pinterest</label>
              <input type="text" name="pinterest" class="form-control" value="{{$data->pinterest}}">
            </div>

            <div class="form-group col-md-4">
              <label for="rss">Rss</label>
              <input type="text" name="rss" class="form-control" value="{{$data->rss}}">
            </div>

            <div class="form-group col-md-4">
              <label for="googleplus">Google-plus</label>
              <input type="text" name="googleplus" class="form-control" value="{{$data->googleplus}}">
            </div>

           
                       
                     <div class="form-group col-md-6" style="padding-top: 30px;">
                       <input type="submit" value="Update" class="btn btn-primary">
                     </div>

                  
                  </div>
                 </form> 
              </div><!-- /.card-body -->
            </div>
          </section>
<!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection