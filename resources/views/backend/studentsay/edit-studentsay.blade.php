@extends('backend.layouts.master')
@section('home_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Review</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Review</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
    
        <!-- Main row -->
        <div class="row">

          <!-- Left col -->
          <section class="col-md-12">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3>Edit Review
                <a href="{{route('studentsay.view')}}" class="btn btn-success btn-sm float-right"><i class="fa fa-list"></i> Review List</a>
                </h3>
              </div><!-- /.card-header -->
              <div class="card-body">
                 <form action="{{route('studentsay.update', $editData->id)}}" method="post" id="myForm" enctype="multipart/form-data">
                   @csrf

					         <div class="form-row">
	                   <div class="form-group col-md-6">
	                       <label for="name">Student Name</label>
	                       <input type="text" name="name" class="form-control" value="{{$editData->name}}">
	                     </div>

	                     <div class="form-group col-md-6">
	                       <label for="title">Title</label>
	                       <input type="text" name="title" class="form-control" value="{{$editData->title}}">
	                     </div>

                        <div class="form-group col-md-6">
                         <label for="message">Message</label>
                         <textarea class="form-control" name="message" cols="4" rows="4" >{{$editData->message}}</textarea>
                       </div>


          						<div class="form-group col-md-4">
          							<label for="image">Image</label>
          							<input type="file" name="image" class="form-control" id="image">
          						</div>

            <div class="form-group col-md-3">
              <img id="showImage" src="{{(!empty($editData->image))?url('public/upload/review/'.$editData->image):url('public/upload/no_image.png')}}" alt="" style="width:150px; height: 160px; border: 1px solid #000;">
            </div>

            <p style="color: #1e0b79">Image size should be (92*92)px and Less then 50kb</p>
	                     
	                     
	                     <div class="form-group col-md-6" style="padding-top: 30px;">
	                       <input type="submit" value="Update" class="btn btn-primary">
	                     </div>

                  
               		</div>
                 </form> 
              </div><!-- /.card-body -->
            </div>
          </section>
<!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script>
$(function () {
  $('#myForm').validate({
    rules: {
      name: {
        required: true,
      },
      title: {
        required: true,
      },
      message: {
        required: true,
      },
    },
    messages: {
     
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection