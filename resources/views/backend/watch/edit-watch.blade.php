@extends('backend.layouts.master')
@section('home_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Watch Campus</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Watch Campus</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
    
        <!-- Main row -->
        <div class="row">

          <!-- Left col -->
          <section class="col-md-12">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3>Edit Watch Campus <a href="{{route('watch.view')}}" class="btn btn-success btn-sm float-right"><i class="fa fa-list"></i> View Watch Campus</a></h3>
              </div><!-- /.card-header -->
              <div class="card-body">
                 <form action="{{route('watch.update',$editData->id)}}" method="post" enctype="multipart/form-data">
                   @csrf
                  <div class="form-row">
                       <div class="form-group col-md-8">
                       <label for="title">Title</label>
                       <input type="text" name="title" value="{{$editData->title}}" class="form-control">
                      </div>

                      <div class="form-group col-md-8">
                       <label for="sub_title">Description</label>
                      <textarea class="form-control" name="sub_title"  cols="5" rows="2" >
                        {{$editData->sub_title}}
                      </textarea>
                    </div> 
                      <!-- <div class="form-group col-md-8">
                         <label for="sub_title">Description</label>
                         <textarea class="form-control" name="sub_title" cols="4" rows="4" >{{$editData->sub_title}}</textarea>
                       </div> -->

                      <div class="form-group col-md-8">
                       <label for="link">Video Link</label>
                       <input type="text" name="link" value="{{$editData->link}}" class="form-control">
                      </div>
           
                       
                       
                     <div class="form-group col-md-6" style="padding-top: 30px;">
                       <input type="submit" value="Update" class="btn btn-primary">
                     </div>

                  
                  </div>
                 </form> 
              </div><!-- /.card-body -->
            </div>
          </section>
<!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection