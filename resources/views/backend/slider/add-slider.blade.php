@extends('backend.layouts.master')
@section('home_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Slider</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Slider</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
    
        <!-- Main row -->
        <div class="row">

          <!-- Left col -->
          <section class="col-md-12">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3>Add Slider
                <a href="{{route('sliders.view')}}" class="btn btn-success btn-sm float-right"><i class="fa fa-list"></i> Slider List</a>
                </h3>
              </div><!-- /.card-header -->
              <div class="card-body">
                 <form action="{{route('sliders.store')}}" method="post" id="myForm" enctype="multipart/form-data">
                   @csrf
					<div class="form-row">

	                   <div class="form-group col-md-6">
	                       <label for="short_title">Short Title</label>
	                       <input type="text" name="short_title" class="form-control">
	                     </div>

	                     <div class="form-group col-md-6">
	                       <label for="long_title">Long  Title</label>
	                       <input type="text" name="long_title" class="form-control">
	                     </div>

						<div class="form-group col-md-4">
							<label for="image">Image</label>
							<input type="file" name="image" class="form-control" id="image">
						</div>

						<div class="form-group col-md-3">
							<img id="showImage" src="{{url('public/upload/no_image.png')}}" alt="" style="width:150px; height: 160px; border: 1px solid #000;">
						</div>
	                     
	                     
	                     <div class="form-group col-md-6" style="padding-top: 30px;">
	                       <input type="submit" value="Submit" class="btn btn-primary">
	                     </div>

                  
               		</div>
                 </form> 
              </div><!-- /.card-body -->
            </div>
          </section>
<!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script>
$(function () {
  $('#myForm').validate({
    rules: {

      short_title: {
        required: true,
      },

      long_title: {
        required: true,
      },

      image: {
        required: true,
      },

    },
    messages: {
      short_title: {
        required: "Please Enter a Short Title",
      },
      long_title: {
        required: "Please Enter a Long Title",
      },
      image: {
        required: "Please select an Image",
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection