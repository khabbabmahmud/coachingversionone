@extends('backend.layouts.master')
@section('home_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Fee Category Ammount</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">Fee Amount</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
    
        <!-- Main row -->
        <div class="row">

          <!-- Left col -->
          <section class="col-md-12">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3>Fee Amount List 
                <a class="btn btn-success float-right btn-sm" href="{{route('setups.studentfeeamount.add')}}"><i class="fa fa-plus-circle"> Add Fee Amount</i></a>
                </h3>
              </div><!-- /.card-header -->
              <div class="card-body">
                  <table id="example2" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>SL.</th>
                      <th>Fee Amount</th>
                      <th>action</th>
                    </tr>
                  </thead>
                  <tbody>

<?php $i = 1; ?>
                    @foreach($data as $value)
                    <tr>
                      <td><?php echo $i++ ?></td>
                      <td>{{$value['fee_category']['name']}}</td>
                      <td>
                        <a title="Details" href="{{route('setups.studentfeeamount.details',$value->fee_category_id)}}" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a>

                        <a title="Edit" href="{{route('setups.studentfeeamount.edit',$value->fee_category_id)}}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div><!-- /.card-body -->
            </div>
          </section>
<!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection