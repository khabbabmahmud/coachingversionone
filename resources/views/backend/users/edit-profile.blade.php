@extends('backend.layouts.master')
@section('home_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Profile</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Profile</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
    
        <!-- Main row -->
        <div class="row">

          <!-- Left col -->
          <section class="col-md-12">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3>Edit Profile
                <a href="{{route('profiles.view')}}" class="btn btn-success btn-sm float-right"><i class="fa fa-book"></i> Your Profile</a>
                </h3>
              </div><!-- /.card-header -->
              <div class="card-body">

                 <form action="{{route('profiles.update')}}" method="post" id="myForm" enctype="multipart/form-data">

                   @csrf
					<div class="form-row">
	                   <div class="form-group col-md-4">
	                       <label for="name">Name</label>
	                       <input type="text" name="name" value="{{$editData->name}}" class="form-control">
	                   </div>

                     <div class="form-group col-md-4">
                         <label for="title">Title</label>
                         <input type="text" name="title" value="{{$editData->title}}" class="form-control">
                       </div>

                       <div class="form-group col-md-4">
                         <label for="address">Address</label>
                         <input type="text" name="address" value="{{$editData->address}}" class="form-control">
                       </div>

                       <div class="form-group col-md-4">
                         <label for="number">Mobile</label>
                         <input type="number" name="mobile" value="{{$editData->mobile}}" class="form-control">
                       </div>
                       <div class="form-group col-md-4">
                         <label for="gender">Gender</label>
                         <select name="gender" id="gender" class="form-control">
                           <option value="">Select Gender</option>
                           <option value="Male" {{($editData->gender=="Male")?"selected":""}}>Male</option>
                           <option value="Female" {{($editData->gender=="Female")?"selected":""}}>Female</option>
                         </select>
                       </div>
                      <div class="form-group col-md-4">
                         <label for="email">Email</label>
                         <input type="email" name="email" value="{{$editData->email}}" class="form-control">
                       </div>

                      <div class="form-group col-md-6">
                         <label for="about">About</label>
                        <textarea class="form-control" name="about" cols="4" rows="4">{{$editData->about}}</textarea>
                       </div>

                       <div class="form-group col-md-6">
                         <label for="qualification">Qualifications</label>
                         <input type="text" name="q1" value="{{$editData->q1}}" class="form-control">
                         <input type="text" name="q2" value="{{$editData->q2}}" class="form-control">
                         <input type="text" name="q3" value="{{$editData->q3}}" class="form-control">
                         <input type="text" name="q4" value="{{$editData->q4}}" class="form-control">
                         <input type="text" name="q5" value="{{$editData->q5}}" class="form-control">
                       </div>

                       <div class="form-group col-md-4">
                         <label for="facebook">Facebook</label>
                         <input type="text" name="fb" value="{{$editData->fb}}" class="form-control">
                       </div>
                       <div class="form-group col-md-4">
                         <label for="twitter">Twitter</label>
                         <input type="text" name="tw" value="{{$editData->tw}}" class="form-control">
                       </div>
                       <div class="form-group col-md-4">
                         <label for="linkedin">Linkedin</label>
                         <input type="text" name="li" value="{{$editData->li}}" class="form-control">
                       </div>

	                     

                        

						<div class="form-group col-md-4">
							<label for="image">Image</label>
              <span style="color:red">For better Performance: Image size recommanded (Width : 368px, Height : 372px) & Less Then 50kb</span>
							<input type="file" name="image" class="form-control" id="image">
						</div>
						<div class="form-group col-md-3">
							<img id="showImage" src="{{(!empty($editData->image))?url('public/upload/user-images/'.$editData->image):url('public/upload/no_image.png')}}" alt="" style="width:150px; height: 160px; border: 1px solid #000;">
						</div>
	                     
	                     
	                     <div class="form-group col-md-6" style="padding-top: 30px;">
	                       <input type="submit" value="Update" class="btn btn-primary">
	                     </div>

                  
               		</div>
                 </form> 
              </div><!-- /.card-body -->
            </div>
          </section>
<!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script>
$(function () {
  $('#myForm').validate({
    rules: {

      name: {
        required: true,
      },

      address: {
        required: true,
      },

      mobile: {
        required: true,
        minlength: 8,
      }, 

      email: {
        required: true,
        email: true,
      },

      gender: {
        required: true,
      },

      
    },
    messages: {
      name: {
        required: "Please Enter user name",
      },
      address: {
        required: "Please Enter Address",
      },
      mobile: {
        required: "Please Enter Mobile Number",
         minlength: "Your Mobile Number must be at least 9 digit",
      },
      email: {
        required: "Please enter a email address",
        email: "Please enter a <em> vaild </em> email address",
      },
      gender: {
        required: "Please Select Your Gender",
       },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection