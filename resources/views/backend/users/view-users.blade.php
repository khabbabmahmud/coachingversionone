@extends('backend.layouts.master')
@section('home_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Users</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Users</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
    
        <!-- Main row -->
        <div class="row">

          <!-- Left col -->
          <section class="col-md-12">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3>Users List
                <a href="{{route('users.add')}}" class="btn btn-success btn-sm float-right"><i class="fa fa-plus-circle"></i> Add User</a>
                </h3>
              </div><!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>SL.</th>
                        <th>Role</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>action</th>
                      </tr>
                    </thead>
                    <tbody>

  <?php $i = 1; ?>
                      @foreach($data as $row)
                      <tr>
                        <td><?php echo $i++ ?></td>
                        <td>{{$row->usertype}}</td>
                        <td>{{$row->name}}</td>
                        <td>{{$row->email}}</td>
                        <td>
                          <a title="Edit" href="{{route('user.edit',$row->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                          <a title="Delete" id="delete" href="{{route('user.delete',$row->id)}}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                        </td>

                      </tr>
                      @endforeach
                    </tbody>
                </table>
              </div><!-- /.card-body -->
            </div>
          </section>
<!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection