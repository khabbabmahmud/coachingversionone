@extends('backend.layouts.master')
@section('home_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Logo</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Logo</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
    
        <!-- Main row -->
        <div class="row">

          <!-- Left col -->
          <section class="col-md-12">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3>Edit Logo <a href="{{route('logos.view')}}" class="btn btn-success btn-sm float-right"><i class="fa fa-list"></i> View Logos</a></h3>
              </div><!-- /.card-header -->
              <div class="card-body">
                
                 <form action="{{route('logos.update',$editData->id)}}" method="post" enctype="multipart/form-data">
                   @csrf
          <div class="form-row">
                       
            <div class="form-group col-md-4">
              <label for="image">Image</label>
              <input type="file" name="image" class="form-control" id="image">
            </div>

            <div class="form-group col-md-3">
              <img id="showImage" src="{{(!empty($editData->image))?url('public/upload/logos/'.$editData->image):url('public/upload/no_image.png')}}" alt="" style="width:200px; height: 55px; border: 1px solid #000; margin-top: 20px;">
            </div>
                       
                       
                     <div class="form-group col-md-6" style="padding-top: 30px;">
                       <input type="submit" value="Update" class="btn btn-primary">
                     </div>

                  
                  </div>
                 </form> 
              </div><!-- /.card-body -->
            </div>
          </section>
<!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection