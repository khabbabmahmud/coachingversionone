@php
$prefix = Request::route()->getPrefix();
$route = Route::current()->getName();
@endphp

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">


@if(Auth::user()->usertype=='Admin')
          <li class="nav-item {{($prefix == '/users')?'menu-open':''}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Manage User
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('users.view')}}" class="nav-link

            @if($route == 'users.view')
                    active
                @elseif($route == 'users.add')
                    active
                @elseif($route == 'users.edit')  
                  active
                @else 

                @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>View User</p>
                </a>
              </li>
              
            </ul>
          </li>
@endif       
        <li class="nav-item {{($prefix == '/profiles')?'menu-open':''}}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Manage Profile
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('profiles.view')}}" class="nav-link 

              @if($route == 'profiles.view')
                    active
                @elseif($route == 'profiles.edit')
                    active
                @else 
                @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Your Profile</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('profiles.password.view')}}" class="nav-link {{($route == 'profiles.password.view')?'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Change Password</p>
                </a>
              </li>
            </ul>
        </li>

<!-- Web Management Start here-->
  <li class="nav-item {{($prefix == '/logos')?'menu-open':''}} {{($prefix == '/sliders')?'menu-open':''}} {{($prefix == '/welcome')?'menu-open':''}} {{($prefix == '/course')?'menu-open':''}} {{($prefix == '/watch')?'menu-open':''}} {{($prefix == '/counter')?'menu-open':''}} {{($prefix == '/contactinfo')?'menu-open':''}} {{($prefix == '/communicate')?'menu-open':''}} {{($prefix == '/faq')?'menu-open':''}} {{($prefix == '/studentsay')?'menu-open':''}} {{($prefix == '/partner')?'menu-open':''}} {{($prefix == '/footertext')?'menu-open':''}}">

            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Web Management
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('communicate.view')}}" class="nav-link 

                @if($route == 'communicate.view')
                    active
                @else 
                @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Communicate</p>
                </a>
              </li>
            </ul>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('contactinfo.view')}}" class="nav-link 

                @if($route == 'contactinfo.view')
                    active
                @else 
                @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Contact Information</p>
                </a>
              </li>
            </ul>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('logos.view')}}" class="nav-link 

                @if($route == 'logos.view')
                    active
                @elseif($route == 'logos.edit')  
                  active
                @else 
                @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Logos</p>
                </a>
              </li>
            </ul>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('sliders.view')}}" class="nav-link

              @if($route == 'sliders.view')
                    active
                @elseif($route == 'sliders.add')
                    active
                @elseif($route == 'sliders.edit')  
                  active
                @else 

                @endif " >
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sliders</p>
                </a>
              </li>
            </ul>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('welcome.view')}}" class="nav-link 

                @if($route == 'welcome.view')
                    active
                @elseif($route == 'welcome.edit')
                    active
                @else 
                @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Welcome To</p>
                </a>
              </li>
            </ul>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('course.view')}}" class="nav-link  
                @if($route == 'course.view')
                    active
                @elseif($route == 'course.add')
                    active
                @elseif($route == 'course.edit')  
                  active
                @else 

                @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Courses</p>
                </a>
              </li>
            </ul>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('watch.view')}}" class="nav-link  
                @if($route == 'watch.view')
                    active
                @elseif($route == 'watch.edit')  
                  active
                @else 

                @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Watch Campus</p>
                </a>
              </li>
            </ul>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('counter.view')}}" class="nav-link  
                @if($route == 'counter.view')
                    active
                @elseif($route == 'counter.edit')  
                  active
                @else 
                @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Counter</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{route('studentsay.view')}}" class="nav-link  
                  @if($route == 'studentsay.view')
                      active
                  @elseif($route == 'studentsay.edit')  
                    active
                  @else 
                  @endif ">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Students Say</p>
                  </a>
                </li>
            </ul>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{route('partner.view')}}" class="nav-link  
                  @if($route == 'partner.view')
                      active
                  @elseif($route == 'partner.edit')  
                    active
                  @else 
                  @endif ">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Partners</p>
                  </a>
                </li>
            </ul>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{route('footertext.view')}}" class="nav-link  
                  @if($route == 'footertext.view')
                      active
                  @endif ">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Footer Text</p>
                  </a>
                </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('faq.view')}}" class="nav-link  
                @if($route == 'faq.view')
                    active
                @elseif($route == 'faq.add')  
                  active
                @else 
                @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>FAQ</p>
                </a>
              </li>
            </ul>

          </li>
<!-- Web Management End here-->     

<!-- Web Management Start here-->
          <li class="nav-item {{($prefix == '/setups')?'menu-open':''}} ">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Setup Management
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('setups.studentclass.view')}}" class="nav-link  
                @if($route == 'setups.studentclass.view')
                    active
                @elseif($route == 'setups.studentclass.add')  
                  active
                @elseif($route == 'setups.studentclass.edit')  
                  active
                @else 

                @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Student Class</p>
                </a>
              </li>
            </ul>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('setups.studentyear.view')}}" class="nav-link  
                @if($route == 'setups.studentyear.view')
                    active
                @elseif($route == 'setups.studentyear.add')  
                  active
                @elseif($route == 'setups.studentyear.edit')  
                  active
                @else 

                @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Year/Sessions</p>
                </a>
              </li>
            </ul>
            
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('setups.studentgroup.view')}}" class="nav-link  
                @if($route == 'setups.studentgroup.view')
                    active
                @elseif($route == 'setups.studentgroup.add')  
                  active
                @elseif($route == 'setups.studentgroup.edit')  
                  active
                @else 

                @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Group/Sections</p>
                </a>
              </li>
            </ul>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('setups.studentshift.view')}}" class="nav-link  
                @if($route == 'setups.studentshift.view')
                    active
                @elseif($route == 'setups.studentshift.add')  
                  active
                @elseif($route == 'setups.studentshift.edit')  
                  active
                @else 

                @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Student Shift</p>
                </a>
              </li>
            </ul>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('setups.studentfeecategory.view')}}" class="nav-link  
                @if($route == 'setups.studentfeecategory.view')
                    active
                @elseif($route == 'setups.studentfeecategory.add')  
                  active
                @elseif($route == 'setups.studentfeecategory.edit')  
                  active
                @else 

                @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Fee Category </p>
                </a>
              </li>
            </ul>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('setups.studentfeeamount.view')}}" class="nav-link  
                @if($route == 'setups.studentfeeamount.view')
                    active
                @elseif($route == 'setups.studentfeeamount.add')  
                  active
                @elseif($route == 'setups.studentfeeamount.edit')  
                  active
                @elseif($route == 'setups.studentfeeamount.details')  
                  active
                @else 

                @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Fee Category Amount</p>
                </a>
              </li>
            </ul>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('setups.examtype.view')}}" class="nav-link  
                @if($route == 'setups.examtype.view')
                    active
                @elseif($route == 'setups.examtype.add')  
                  active
                @elseif($route == 'setups.examtype.edit')  
                  active
                @else 

                @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Exam Type</p>
                </a>
              </li>
            </ul>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('setups.subject.view')}}" class="nav-link  
                @if($route == 'setups.subject.view')
                    active
                @elseif($route == 'setups.subject.add')  
                  active
                @elseif($route == 'setups.subject.edit')  
                  active
                @else 

                @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Subject View</p>
                </a>
              </li>
            </ul>



          </li>
<!-- Web Management End here-->     



        </ul>
      </nav>
      <!-- /.sidebar-menu -->


