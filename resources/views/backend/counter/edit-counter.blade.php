@extends('backend.layouts.master')
@section('home_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Counter</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Counter</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
    
        <!-- Main row -->
        <div class="row">

          <!-- Left col -->
          <section class="col-md-12">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3>Edit Counter <a href="{{route('counter.view')}}" class="btn btn-success btn-sm float-right"><i class="fa fa-list"></i> View Counter</a></h3>
              </div><!-- /.card-header -->
              <div class="card-body">
                 <form action="{{route('counter.update',$editData->id)}}" method="post" enctype="multipart/form-data">
                   @csrf
          <div class="form-row">
                       
            <div class="form-group col-md-4">
              <label for="one">Number One</label>
              <input type="number" name="one" class="form-control" value="{{$editData->one}}">
            </div>
            <div class="form-group col-md-4">
              <label for="one_title">Title One</label>
              <input type="text" name="one_title" class="form-control" value="{{$editData->one_title}}">
            </div>

            <div class="form-group col-md-4">
              <label for="two">Two One</label>
              <input type="number" name="two" class="form-control" value="{{$editData->two}}">
            </div>
            <div class="form-group col-md-4">
              <label for="two_title">Title Two</label>
              <input type="text" name="two_title" class="form-control" value="{{$editData->two_title}}">
            </div><div class="form-group col-md-4">
              <label for="three">Number Three</label>
              <input type="number" name="three" class="form-control" value="{{$editData->three}}">
            </div>
            <div class="form-group col-md-4">
              <label for="three_title">Title Three</label>
              <input type="text" name="three_title" class="form-control" value="{{$editData->three_title}}">
            </div><div class="form-group col-md-4">
              <label for="four">Number Four</label>
              <input type="number" name="four" class="form-control" value="{{$editData->four}}">
            </div>
            <div class="form-group col-md-4">
              <label for="four_title">Title Four</label>
              <input type="text" name="four_title" class="form-control" value="{{$editData->four_title}}">
            </div>

            
                       
                       
                     <div class="form-group col-md-6" style="padding-top: 30px;">
                       <input type="submit" value="Update" class="btn btn-primary">
                     </div>

                  
                  </div>
                 </form> 
              </div><!-- /.card-body -->
            </div>
          </section>
<!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection