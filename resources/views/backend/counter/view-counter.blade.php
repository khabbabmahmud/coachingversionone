@extends('backend.layouts.master')
@section('home_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Counter</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">Counter</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
    
        <!-- Main row -->
        <div class="row">

          <!-- Left col -->
          <section class="col-md-12">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3>Counter List </h3>
              </div><!-- /.card-header -->
              <div class="card-body">
                  <table id="example2" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>SL.</th>

                      <th>Number One</th>
                      <th>Title One</th>
                      <th>Number Two</th>
                      <th>Title Two</th>
                      <th>Number Three</th>
                      <th>Title Three</th>
                      <th>Number Four</th>
                      <th>Title Four</th>
                      
                      <th>action</th>
                    </tr>
                  </thead>
                  <tbody>

<?php $i = 1; ?>
                    @foreach($data as $row)
                    <tr>
                      <td><?php echo $i++ ?></td>
                      <td>{{$row->one}}</td>
                      <td>{{$row->one_title}}</td>
                      <td>{{$row->two}}</td>
                      <td>{{$row->two_title}}</td>
                      <td>{{$row->three}}</td>
                      <td>{{$row->three_title}}</td>
                      <td>{{$row->four}}</td>
                      <td>{{$row->four_title}}</td>
                     

                      <td>
                        <a title="Edit" href="{{route('counter.edit',$row->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                      </td>

                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div><!-- /.card-body -->
            </div>
          </section>
<!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection