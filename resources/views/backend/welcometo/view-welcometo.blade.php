@extends('backend.layouts.master')
@section('home_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Welcome section</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">Welcome To</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
    
        <!-- Main row -->
        <div class="row">

          <!-- Left col -->
          <section class="col-md-12">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3>welcometo List </h3>
              </div><!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                  <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>SL.</th>
                      <th>Title</th>
                      <th>Sub Title</th>
                      <th>One Title</th>
                      <th>One Sub</th>
                      <th>Two Title</th>
                      <th>Two Sub</th>
                      <th>Three Title</th>
                      <th>Three Sub</th>
                      <th>action</th>
                    </tr>
                  </thead>
                  <tbody>

<?php $i = 1; ?>
                    @foreach($data as $row)
                    <tr>
                      <td><?php echo $i++ ?></td>
                      <td>{{$row->short_title}}</td>
                      <td>{{$row->long_title}}</td>
                      <td>{{$row->sone_title}}</td>
                      <td>{{$row->sone_subtitle}}</td>
                      <td>{{$row->stwo_title}}</td>
                      <td>{{$row->stwo_subtitle}}</td>
                      <td>{{$row->sthree_title}}</td>
                      <td>{{$row->sthree_subtitle}}</td>

                      

                      <td>
                        <a title="Edit" href="{{route('welcome.edit',$row->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                      </td>

                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div><!-- /.card-body -->
            </div>
          </section>
<!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection