@extends('backend.layouts.master')
@section('home_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Welcome To</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Welcome To</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
    
        <!-- Main row -->
        <div class="row">

          <!-- Left col -->
          <section class="col-md-12">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3>Edit Welcome To
                <a href="{{route('welcome.view')}}" class="btn btn-success btn-sm float-right"><i class="fa fa-false"></i> Back</a>
                </h3>
              </div><!-- /.card-header -->
              <div class="card-body">
                 <form action="{{route('welcome.update',$editData->id)}}" method="post" id="myForm" enctype="multipart/form-data">
                   @csrf
          <div class="form-row">

                     <div class="form-group col-md-6">
                         <label for="short_title">Short Title</label>
                         <input type="text" name="short_title" value="{{$editData->short_title}}" class="form-control">
                       </div>

                       <div class="form-group col-md-6">
                         <label for="long_title">Long  Title</label>
                         <textarea class="form-control" name="long_title" cols="4" rows="4" >{{$editData->long_title}}</textarea>
                        
                       </div>

                       <div class="form-group col-md-6">
                         <label for="sone_title">One Title</label>
                         <input type="text" name="sone_title" value="{{$editData->sone_title}}" class="form-control">
                       </div>

                       <div class="form-group col-md-6">
                         <label for="sone_subtitle">One Sub Title</label>
                         <textarea class="form-control" name="sone_subtitle" cols="4" rows="4" >{{$editData->sone_subtitle}}</textarea>
                       </div>

                       <div class="form-group col-md-6">
                         <label for="stwo_title">Two Title</label>
                         <input type="text" name="stwo_title" value="{{$editData->stwo_title}}" class="form-control">
                       </div>

                       <div class="form-group col-md-6">
                         <label for="stwo_subtitle">Two Sub Title</label>
                         <textarea class="form-control" name="stwo_subtitle" cols="4" rows="4" >{{$editData->stwo_subtitle}}</textarea>
                       </div>

                       <div class="form-group col-md-6">
                         <label for="sthree_title">Three Title</label>
                         <input type="text" name="sthree_title" value="{{$editData->sthree_title}}" class="form-control">
                       </div>

                       <div class="form-group col-md-6">
                         <label for="sthree_subtitle">Three sub Title</label>
                           <textarea class="form-control" name="sthree_subtitle" cols="4" rows="4" >{{$editData->sthree_subtitle}}</textarea>
                       </div>

                       
                       
                       
                       <div class="form-group col-md-6" style="padding-top: 30px;">
                         <input type="submit" value="Update" class="btn btn-primary">
                       </div>

                  
                  </div>
                 </form> 
              </div><!-- /.card-body -->
            </div>
          </section>
<!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script>
$(function () {
  $('#myForm').validate({
    rules: {

      short_title: {
        required: true,
      },

      long_title: {
        required: true,
      },

      sone_title: {
        required: true,
      },
      sone_subtitle: {
        required: true,
      },
      stwo_title: {
        required: true,
      },

      stwo_subtitle: {
        required: true,
      },
      sthree_title: {
        required: true,
      },

      sthree_subtitle: {
        required: true,
      },

     
    },
    messages: {
      short_title: {
        required: "Please Enter a Short Title",
      },
      long_title: {
        required: "Please Enter a Long Title",
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection