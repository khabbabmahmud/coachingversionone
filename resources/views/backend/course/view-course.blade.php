@extends('backend.layouts.master')
@section('home_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Course</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">Course</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
    
        <!-- Main row -->
        <div class="row">

          <!-- Left col -->
          <section class="col-md-12">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3>Course List 
                <a class="btn btn-success float-right btn-sm" href="{{route('course.add')}}"><i class="fa fa-plus-circle"> Add Course</i></a>
                </h3>
              </div><!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                  <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>SL.</th>
                      <th>Image</th>
                      <th style="color: red;">info</th>
                      <th>Course Title</th>
                      <th>Sub Title</th>
                      <th>Duration</th>
                      <th>Total Classess</th>
                      <th>Class Time</th>
                      <th>action</th>
                    </tr>
                  </thead>
                  <tbody>

<?php $i = 1; ?>
                    @foreach($data as $row)
                    <tr>
                      <td><?php echo $i++ ?></td>

                      <td>
                        <img src="{{(!empty($row->image))?url('public/upload/course/'.$row->image):url('public/upload/no_image.png') }}" alt="" style="width:100px; height: 55px; border: 1px solid #000;">
                      </td>

                       <td>Size Should be(500*360)px and less then 50kb.</td>
  					           <td>{{$row->short_title}}</td>
                       <td>{{$row->long_title}}</td>
                       <td>{{$row->duration}}</td>
                       <td>{{$row->classess}}</td>
  					           <td>{{$row->class_time}}</td>

                      <td>
                        <a title="Edit" href="{{route('course.edit',$row->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                        
                         <a title="Delete" id="delete" href="{{route('course.delete',$row->id)}}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                      </td>

                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div><!-- /.card-body -->
            </div>
          </section>
<!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection