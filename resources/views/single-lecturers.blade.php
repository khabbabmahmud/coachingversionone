@extends('welcome')

@section('single_lecturers_content')
<!-- Inner Page Banner Area Start Here -->
        <div class="inner-page-banner-area" style="background-image: url('{{asset('public/frontend/img/banner/5.jpg')}}');">
            <div class="container">
                <div class="pagination-area">
                    <h1>Lecturers Details</h1>
                    <ul>
                        <li><a href="#">Home</a> -</li>
                        <li>Details</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Inner Page Banner Area End Here -->

        <!-- lacturers Page Area Start Here -->
        <div class="lecturers-page-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="lecturers-contact-info">
                            <img src="{{asset('public/upload/user-images/'.$data->image)}}" class="img-responsive" alt="team">
                            <h2>{{$data->name}}</h2>
                            <h3>{{$data->title}}</h3>
                            <ul class="lecturers-social2">
                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            </ul>
                            <ul class="lecturers-contact">
                                <li><i class="fa fa-phone" aria-hidden="true"></i>{{$data->mobile}}</li>
                                <li><i class="fa fa-envelope-o" aria-hidden="true"></i>{{$data->email}}</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                        <h3 class="title-default-left title-bar-big-left-close">About Me</h3>
                        <p>{{$data->about}}</p>
                        <h3 class="title-default-left title-bar-big-left-close">Qualifications</h3>
                        <ul class="course-feature2">
                            <li>{{$data->q1}}</li>
                            <li>{{$data->q2}}</li>
                            <li>{{$data->q3}}</li>
                            <li>{{$data->q4}}</li>
                            <li>{{$data->q5}}</li>
                        </ul>
                        <div class="leave-comments">
                            <h3 class="title-default-left title-bar-big-left-close">Contact With Me</h3>
                            <div class="row">
                                <div class="contact-form">
                                    <form>
                                        <fieldset>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" placeholder="Name" class="form-control">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input type="email" placeholder="Email" class="form-control">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <textarea placeholder="Comment" class="textarea form-control" id="form-message" rows="8" cols="20"></textarea>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group margin-bottom-none">
                                                    <button type="submit" class="view-all-accent-btn">Post Comment</button>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- lacturers Page Area End Here -->
@endsection