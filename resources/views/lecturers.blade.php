@extends('welcome')
@section('lecturers_container')

<!-- Inner Page Banner Area Start Here -->
<div class="inner-page-banner-area" style="background-image: url('{{asset('public/frontend/img/banner/5.jpg')}}');">
    <div class="container">
        <div class="pagination-area">
            <h1>Our Lecturers</h1>
            <ul>
                <li><a href="#">Home</a> - </li>
                <li>Lecturers</li>
            </ul>
        </div>
    </div>
</div>
<!-- Inner Page Banner Area End Here -->


<!-- Lecturers Page Area Start Here -->
<div class="lecturers-page1-area">
    <div class="container">
        <div class="row">

@foreach($lacturars as $lacturar)
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="single-item">
                    <div class="lecturers1-item-wrapper">
                        <div class="lecturers-img-wrapper">
                            <a href="{{route('single_lecturer',$lacturar->id)}}"><img class="img-responsive" src="{{asset('public/upload/user-images/'.$lacturar->image)}}" alt="team"></a>
                        </div>
                        <div class="lecturers-content-wrapper">
                            <h3 class="item-title"><a href="{{route('single_lecturer',$lacturar->id)}}">{{$lacturar->name}}</a></h3>
                            <span class="item-designation">{{$lacturar->title}}</span>
                            <ul class="lecturers-social">
                              
                                <li><a target="_blank" href="{{$lacturar->li}}"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>

                                <li><a target="_blank" href="{{$lacturar->tw}}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>

                                <li><a target="_blank" href="{{$lacturar->fb}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
@endforeach
        </div>


    </div>
</div>
<!-- Lecturers Page  Area End Here -->

@endsection