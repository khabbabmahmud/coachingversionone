@extends('welcome')
@section('slider')
@php
$count = 1;
@endphp
        <!-- Slider 1 Area Start Here -->
        <div class="slider1-area overlay-default">
            <div class="bend niceties preview-1">

                <div id="ensign-nivoslider-3" class="slides">
                    @foreach($sliders as $slider)
                    <img src="{{asset('public/upload/sliders/'.$slider->image)}}" alt="slider" title="#slider-direction-{{$count++}}" />
                
                    @endforeach
                </div>

@php
$count2 = 1;
@endphp
@php
$count3 = 1;
@endphp
            @foreach($sliders as $slider)
                <div id="slider-direction-{{$count2++}}" class="t-cn slider-direction">
                    <div class="slider-content s-tb slide-{{$count3++}}">
                        <div class="title-container s-tb-c">
                            <div class="title1">{{$slider->short_title}}</div>
                            <p>{{$slider->long_title}}</p>
                            <div class="slider-btn-area">
                                <a href="#course" class="default-big-btn">Start a Course</a>
                            </div>
                        </div>
                    </div>
                </div>

                @endforeach

                </div>

            </div>
        </div>
        <!-- Slider 1 Area End Here -->
@endsection

@section('about_area')
        <!-- About Area Start Here -->
        <div class="about2-area">
            <div class="container">
                <h1 class="about-title">{{$welcometo->short_title}}</h1>
                <p class="about-sub-title">{{$welcometo->long_title}}</p>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeIn" data-wow-duration="2s" data-wow-delay=".1s">
                        <div class="service-box2">
                            <div class="service-box-icon">
                                <a href="#"><i class="fa fa-graduation-cap" aria-hidden="true"></i></a>
                            </div>
                            <h3><a href="#">{{$welcometo->sone_title}}</a></h3>
                            <p> {{$welcometo->sone_subtitle}}</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeIn" data-wow-duration="2s" data-wow-delay=".4s">
                        <div class="service-box2">
                            <div class="service-box-icon">
                                <a href="#"><i class="fa fa-user" aria-hidden="true"></i></a>
                            </div>
                            <h3><a href="#">{{$welcometo->stwo_title}}</a></h3>
                            <p> {{$welcometo->stwo_subtitle}}</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeIn" data-wow-duration="2s" data-wow-delay=".7s">
                        <div class="service-box2">
                            <div class="service-box-icon">
                                <a href="#"><i class="fa fa-book" aria-hidden="true"></i></a>
                            </div>
                            <h3><a href="#">{{$welcometo->sthree_title}}</a></h3>
                            <p>{{$welcometo->sthree_subtitle}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About  Area End Here -->
@endsection

@section('featured_area')
        <!-- Featured Area Start Here -->

        <!-- Courses 1 Area Start Here -->
        <div class="courses1-area" id="course">
            <div class="container">
                <h2 class="title-default-left">Our Courses</h2>
            </div>
            <div id="shadow-carousel" class="container">
                <div class="rc-carousel" data-loop="true" data-items="4" data-margin="20" data-autoplay="false" data-autoplay-timeout="10000" data-smart-speed="2000" data-dots="false" data-nav="true" data-nav-speed="false" data-r-x-small="1" data-r-x-small-nav="true" data-r-x-small-dots="false" data-r-x-medium="2" data-r-x-medium-nav="true" data-r-x-medium-dots="false" data-r-small="2" data-r-small-nav="true" data-r-small-dots="false" data-r-medium="3" data-r-medium-nav="true" data-r-medium-dots="false" data-r-large="4" data-r-large-nav="true" data-r-large-dots="false">
                
        @foreach($courses as $course)
                    <div class="courses-box1">
                        <div class="single-item-wrapper">
                            <div class="courses-img-wrapper hvr-bounce-to-bottom">
                                <img class="img-responsive" src="{{asset('public/upload/course/'.$course->image)}}" alt="courses">
                                <a href="#course"><i class="fa fa-link" aria-hidden="true"></i></a>
                            </div>
                            <div class="courses-content-wrapper">
                                <h3 class="item-title"><a href="#">{{$course->short_title}}</a></h3>
                                <p class="item-content">{{$course->long_title}}</p>
                                <ul class="courses-info">
                                    <li>{{$course->duration}}
                                        <br><span> Course</span></li>
                                    <li>{{$course->classess}}
                                        <br><span> Classes</span></li>
                                    <li>{{$course->class_time}}
                                        <br><span> Time</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
            @endforeach

                </div>
            </div>
        </div>
        <!-- Courses 1 Area End Here -->

        
@endsection
<!-- Featured Area End Here -->

<!-- Lecturers Area Start Here -->
@section('lecturers_area')

        
        <div class="lecturers-area">
            <div class="container">
                <h2 class="title-default-left">Our Skilled Lecturers</h2>
            </div>
            <div class="container">
                <div class="rc-carousel" data-loop="true" data-items="4" data-margin="30" data-autoplay="true" data-autoplay-timeout="10000" data-smart-speed="2000" data-dots="false" data-nav="true" data-nav-speed="false" data-r-x-small="1" data-r-x-small-nav="true" data-r-x-small-dots="false" data-r-x-medium="2" data-r-x-medium-nav="true" data-r-x-medium-dots="false" data-r-small="3" data-r-small-nav="true" data-r-small-dots="false" data-r-medium="4" data-r-medium-nav="true" data-r-medium-dots="false" data-r-large="4" data-r-large-nav="true" data-r-large-dots="false">
                   
            @foreach($lacturars as $lacturar)
                    <div class="single-item">
                        <div class="lecturers1-item-wrapper">

                            <div class="lecturers-img-wrapper">
                                <a href="{{route('single_lecturer',$lacturar->id)}}"><img class="img-responsive" src="{{asset('public/upload/user-images/'.$lacturar->image)}}" alt="team"></a>
                            </div>
                            
                            <div class="lecturers-content-wrapper">
                                <h3 class="item-title"><a href="{{route('single_lecturer',$lacturar->id)}}">{{$lacturar->name}}</a></h3>

                                <span class="item-designation">{{$lacturar->title}}</span>
                                <ul class="lecturers-social">

                                    <li><a target="_blank" href="{{$lacturar->li}}"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>

                                    <li><a target="_blank" href="{{$lacturar->tw}}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>

                                    <li><a target="_blank" href="{{$lacturar->fb}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                                </ul>
                            </div>
                        </div>
                    </div>
            @endforeach
                </div>
            </div>
        </div>
   
@endsection
<!-- Lecturers Area End Here -->

<!-- Video Area Start Here -->
@section('video_area')
        
        <div class="video-area overlay-video bg-common-style" style="background-image: url('{{asset('public/frontend/img/banner/1.jpg')}}');">
            <div class="container">
                <div class="video-content">
                    <h2 class="video-title">{{$watch->title}}</h2>
                    <p class="video-sub-title">{{$watch->sub_title}}</p>
                    <a class="play-btn popup-youtube wow bounceInUp" data-wow-duration="2s" data-wow-delay=".1s" href="http://www.youtube.com/watch?v=1iIZeIy7TqM"><i class="fa fa-play" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
        
@endsection
<!-- Video Area End Here -->

<!-- News and Event Area Start Here -->
@section('news_event_area')
        
       <!--  <div class="news-event-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 news-inner-area">
                        <h2 class="title-default-left">Latest News</h2>
                        <ul class="news-wrapper">
                            <li>
                                <div class="news-img-holder">
                                    <a href="#"><img src="{{asset('public/frontend/img/news/1.jpg')}}" class="img-responsive" alt="news"></a>
                                </div>
                                <div class="news-content-holder">
                                    <h3><a href="single-news.html">Summer Course Start From 1st June</a></h3>
                                    <div class="post-date">June 15, 2021</div>
                                    <p>Pellentese turpis dignissim amet area ducation process facilitating Knowledge.</p>
                                </div>
                            </li>
                            <li>
                                <div class="news-img-holder">
                                    <a href="#"><img src="{{asset('public/frontend/img/news/2.jpg')}}" class="img-responsive" alt="news"></a>
                                </div>
                                <div class="news-content-holder">
                                    <h3><a href="single-news.html">Guest Interview will Occur Soon</a></h3>
                                    <div class="post-date">June 25, 2021</div>
                                    <p>Pellentese turpis dignissim amet area ducation process facilitating Knowledge.</p>
                                </div>
                            </li>
                            <li>
                                <div class="news-img-holder">
                                    <a href="#"><img src="{{asset('public/frontend/img/news/3.jpg')}}" class="img-responsive" alt="news"></a>
                                </div>
                                <div class="news-content-holder">
                                    <h3><a href="single-news.html">Easy English Learning Way</a></h3>
                                    <div class="post-date">June 30, 2021</div>
                                    <p>Pellentese turpis dignissim amet area ducation process facilitating Knowledge.</p>
                                </div>
                            </li>
                        </ul>
                        <div class="news-btn-holder">
                            <a target="_blank" href="{{route('news')}}" class="view-all-accent-btn">View All</a>
                        </div>
                    </div>
                    
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 event-inner-area">
                        <h2 class="title-default-left">Upcoming Events</h2>
                        <ul class="event-wrapper">
                            <li class="wow bounceInUp" data-wow-duration="2s" data-wow-delay=".1s">
                                <div class="event-calender-wrapper">
                                    <div class="event-calender-holder">
                                        <h3>26</h3>
                                        <p>April</p>
                                        <span>2021</span>
                                    </div>
                                </div>
                                <div class="event-content-holder">
                                    <h3><a href="single-event.html">Freashers MeetUp Conference 2021</a></h3>
                                    <p>Pellentese turpis dignissim amet area ducation process facilitating Knowledge. Pellentese turpis dignissim amet area ducation process facilitating Knowledge. Pellentese turpis dignissim amet area ducation.</p>
                                    <ul>
                                        <li>04:00 PM - 06:00 PM</li>
                                        <li>Australia , Melborn</li>
                                    </ul>
                                </div>
                            </li>
                            <li class="wow bounceInUp" data-wow-duration="2s" data-wow-delay=".3s">
                                <div class="event-calender-wrapper">
                                    <div class="event-calender-holder">
                                        <h3>26</h3>
                                        <p>May</p>
                                        <span>2021</span>
                                    </div>
                                </div>
                                <div class="event-content-holder">
                                    <h3><a href="single-event.html">PSDC Study Tour</a></h3>
                                    <p>Pellentese turpis dignissim amet area ducation process facilitating Knowledge. Pellentese turpis dignissim amet area ducation process facilitating Knowledge. Pellentese turpis dignissim amet area ducation.</p>
                                    <ul>
                                        <li>03:00 PM - 05:00 PM</li>
                                        <li>Australia , Melborn</li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                        <div class="event-btn-holder">
                            <a target="_blank" href="{{route('event')}}" class="view-all-primary-btn">View All</a>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        
@endsection
<!-- News and Event Area End Here -->



<!-- Students Say Area Start Here --> 
@section('student_say_area')

        <div class="students-say-area">
            <h2 class="title-default-center">What Our Students Say</h2>
            <div class="container">
                <div class="rc-carousel" data-loop="true" data-items="2" data-margin="30" data-autoplay="false" data-autoplay-timeout="10000" data-smart-speed="2000" data-dots="true" data-nav="false" data-nav-speed="false" data-r-x-small="1" data-r-x-small-nav="false" data-r-x-small-dots="true" data-r-x-medium="2" data-r-x-medium-nav="false" data-r-x-medium-dots="true" data-r-small="2" data-r-small-nav="false" data-r-small-dots="true" data-r-medium="2" data-r-medium-nav="false" data-r-medium-dots="true" data-r-large="2" data-r-large-nav="false" data-r-large-dots="true">

                @foreach($studentsays as $studentsay)
                    <div class="single-item">
                        <div class="single-item-wrapper">
                            <div class="profile-img-wrapper">
                                <a href="#" class="profile-img"><img class="profile-img-responsive img-circle" src="{{asset('public/upload/review/'.$studentsay->image)}}" alt="Testimonial"></a>
                            </div>
                            <div class="tlp-tm-content-wrapper">
                                <h3 class="item-title"><a href="#">{{$studentsay->name}}</a></h3>
                                <span class="item-designation">{{$studentsay->title}}</span>
                                <ul class="rating-wrapper">
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                </ul>
                                <div class="item-content">{{$studentsay->message}}</div>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
        
 @endsection
<!-- Students Say Area End Here -->



<!-- Counter Area Start Here -->
@section('counter_area')
        
        <div class="counter-area bg-primary-deep" style="background-image: url('{{asset('public/frontend/img/banner/4.jpg')}}');">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 counter1-box wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".20s">
                        <h2 class="about-counter title-bar-counter" data-num="{{$counter->one}}">{{$counter->one}}</h2>
                        <p>{{$counter->one_title}}</p>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 counter1-box wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".40s">
                        <h2 class="about-counter title-bar-counter" data-num="{{$counter->two}}">{{$counter->two}}</h2>
                        <p>{{$counter->two_title}}</p>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 counter1-box wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".60s">
                        <h2 class="about-counter title-bar-counter" data-num="{{$counter->three}}">{{$counter->three}}</h2>
                        <p>{{$counter->three_title}}</p>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 counter1-box wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".80s">
                        <h2 class="about-counter title-bar-counter" data-num="{{$counter->four}}">{{$counter->four}}</h2>
                        <p>{{$counter->four_title}}</p>
                    </div>

                </div>
            </div>
        </div>
       
@endsection
 <!-- Counter Area End Here -->





<!-- Students Join Area Start Here -->
@section('student_join_area')
        
        <div class="students-join2-area">
            <div class="container">
                <div class="students-join2-wrapper">
                    <div class="students-join2-left">
                        <div id="ri-grid" class="author-banner-bg ri-grid header text-center">
                            <ul class="ri-grid-list">
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/1.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/2.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/3.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/4.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/5.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/6.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/7.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/8.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/9.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/10.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/11.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/12.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/13.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/14.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/15.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/16.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/5.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/6.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/1.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/2.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/3.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/4.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/5.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/6.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/6.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/1.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/2.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/3.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/4.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/5.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/6.jpg')}}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{asset('public/frontend/img/students/1.jpg')}}" alt=""></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="students-join2-right">
                        <div>
                            <h2>Join<span> 29,12,093</span> Students.</h2>
                            <a target="_blank" href="{{route('registration')}}" class="join-now-primary-btn">Join Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
@endsection
<!-- Students Join 2 Area End Here -->

@section('brand_area')
        <!-- Brand Area Start Here -->
        <div class="brand-area">
            <div class="container">
                <div class="rc-carousel" data-loop="true" data-items="4" data-margin="30" data-autoplay="true" data-autoplay-timeout="5000" data-smart-speed="2000" data-dots="false" data-nav="false" data-nav-speed="false" data-r-x-small="2" data-r-x-small-nav="false" data-r-x-small-dots="false" data-r-x-medium="3" data-r-x-medium-nav="false" data-r-x-medium-dots="false" data-r-small="4" data-r-small-nav="false" data-r-small-dots="false" data-r-medium="4" data-r-medium-nav="false" data-r-medium-dots="false" data-r-large="4" data-r-large-nav="false" data-r-large-dots="false">
                
                @foreach($partners as $partner)
                    <div class="brand-area-box">
                        <a href="#"><img src="{{asset('public/upload/partner/'.$partner->image)}}" alt="brand"></a>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
        <!-- Brand Area End Here -->
@endsection







