@extends('welcome')

@section('faq_content')

<!-- Inner Page Banner Area Start Here -->
<div class="inner-page-banner-area" style="background-image: url('{{asset('public/frontend/img/banner/5.jpg')}}');">
    <div class="container">
        <div class="pagination-area">
            <h1>Frequently Asked Questions</h1>
            <ul>
                <li><a href="#">Home</a> -</li>
                <li>Faq</li>
            </ul>
        </div>
    </div>
</div>
<!-- Inner Page Banner Area End Here -->

<!-- Faq Page Area Start Here -->
<div class="faq-page-area">
    <div class="container">
        <div class="row panel-group" id="faq-accordian">
            


            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @php 
        $countfaq = 1;
        @endphp
        @foreach($faqs as $faq)
      
                <div class="faq-box-wrapper">
                    <div class="faq-box-item panel panel-default">
                        <div class="panel-heading @if($countfaq == 1){active} @endif">
                            <div class="panel-title faq-box-title">
                                <h3>
                                        <a aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#{{$faq->id}}"><span class="faq-box-count">{{$countfaq}}</span>{{$faq->question}}
                                        </a>
                                    </h3>
                            </div>
                        </div>
                        <div aria-expanded="false" id="{{$faq->id}}" role="tabpanel" class="panel-collapse collapse @if($countfaq == 1){in} @endif">
                            <div class="panel-body faq-box-body">
                                <p>{{$faq->ans}}</p>
                            </div>
                        </div>
                    </div>
                </div>
        @php 
        $countfaq++
        @endphp   
        @endforeach
               <!--  <div class="faq-box-wrapper">
                    <div class="faq-box-item panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title faq-box-title">
                                <h3>
                                        <a aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="faq-box-count">2</span>Plummy text  printing and typesetting iyourndustry.
                                        </a>
                                    </h3>
                            </div>
                        </div>
                        <div aria-expanded="false" id="collapseTwo" role="tabpanel" class="panel-collapse collapse">
                            <div class="panel-body faq-box-body">
                                <p>Dorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuriesthe leap into electronic.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="faq-box-wrapper">
                    <div class="faq-box-item panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title faq-box-title">
                                <h3>
                                        <a aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="faq-box-count">3</span>Plummy text  printing and typesetting iyourndustry.
                                        </a>
                                    </h3>
                            </div>
                        </div>
                        <div aria-expanded="false" id="collapseThree" role="tabpanel" class="panel-collapse collapse">
                            <div class="panel-body faq-box-body">
                                <p>Dorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuriesthe leap into electronic.</p>
                            </div>
                        </div>
                    </div>
                </div> -->

            </div>


           <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="faq-box-wrapper">
                    <div class="faq-box-item panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title faq-box-title">
                                <h3>
                                        <a aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="faq-box-count">4</span>Plummy text  printing and typesetting iyourndustry.
                                        </a>
                                    </h3>
                            </div>
                        </div>
                        <div aria-expanded="false" id="collapseFour" role="tabpanel" class="panel-collapse collapse">
                            <div class="panel-body faq-box-body">
                                <p>Dorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuriesthe leap into electronic.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="faq-box-wrapper">
                    <div class="faq-box-item panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title faq-box-title">
                                <h3>
                                        <a aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><span class="faq-box-count">5</span>Plummy text  printing and typesetting iyourndustry.
                                        </a>
                                    </h3>
                            </div>
                        </div>
                        <div aria-expanded="false" id="collapseFive" role="tabpanel" class="panel-collapse collapse">
                            <div class="panel-body faq-box-body">
                                <p>Dorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuriesthe leap into electronic.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="faq-box-wrapper">
                    <div class="faq-box-item panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title faq-box-title">
                                <h3>
                                        <a aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><span class="faq-box-count">6</span>Plummy text  printing and typesetting iyourndustry.
                                        </a>
                                    </h3>
                            </div>
                        </div>
                        <div aria-expanded="false" id="collapseSix" role="tabpanel" class="panel-collapse collapse">
                            <div class="panel-body faq-box-body">
                                <p>Dorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuriesthe leap into electronic.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>
<!-- Faq Page Area End Here -->
        
@endsection

