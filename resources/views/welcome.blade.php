<!doctype html>
<html class="no-js" lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <meta name="author" content="softkhabbab">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Practicing Science Dot Com</title>
    <meta name="description" content="Best Coaching Center in Bangladesh">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/frontend/img/favicon.png')}}">
    <!-- Normalize CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/css/normalize.css')}}">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/css/main.css')}}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/css/bootstrap.min.css')}}">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/css/animate.min.css')}}">
    <!-- Font-awesome CSS-->
    <link rel="stylesheet" href="{{asset('public/frontend/css/font-awesome.min.css')}}">
    <!-- Owl Caousel CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/vendor/OwlCarousel/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/frontend/vendor/OwlCarousel/owl.theme.default.min.css')}}">
    <!-- Main Menu CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/css/meanmenu.min.css')}}">
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/vendor/slider/css/nivo-slider.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('public/frontend/vendor/slider/css/preview.css')}}" type="text/css" media="screen" />
    <!-- Datetime Picker Style CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/css/jquery.datetimepicker.css')}}">
    <!-- Magic popup CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/css/magnific-popup.css')}}">
    <!-- Switch Style CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/css/hover-min.css')}}">
    <!-- ReImageGrid CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/css/reImageGrid.css')}}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/style.css?var=1.1')}}">
    <!-- Modernizr Js -->

    <!-- toaster css -->
    <link rel="stylesheet" type="text/css" href="{{asset('/public/frontend/toastr/toaster.min.css')}}">

    <script src="{{asset('public/frontend/js/modernizr-2.8.3.min.js')}}"></script>
    <!-- jQuery -->
<script src="{{asset('public/backend')}}/plugins/jquery/jquery.min.js"></script>
</head>

<body>
   
    <!-- Preloader Start Here -->
    <div id="preloader"></div>
    <!-- Preloader End Here -->
    <!-- Main Body Area Start Here -->
    <div id="wrapper">
        <!-- Header Area Start Here -->
        <header>
            <div id="header2" class="header2-area">
                <div class="header-top-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="header-top-left">
                                    <ul>
                                        <li><i class="fa fa-phone" aria-hidden="true"></i><a href="#">{{$contactinfo->number}}</a></li>
                                        <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="#">{{$contactinfo->email}}</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="header-top-right">
                                    <ul>
                                        <li>
                                            <a class="login-btn-area" href="#" id="login-button"><i class="fa fa-lock" aria-hidden="true"></i> Login</a>
                                            <div class="login-form" id="login-form" style="display: none;">
                                                <div class="title-default-left-bold">Login</div>
                                                <form>
                                                    <label>Username or email address *</label>
                                                    <input type="text" placeholder="Name or E-mail" />
                                                    <label>Password *</label>
                                                    <input type="password" placeholder="Password" />
                                                    <label class="check">Lost your password?</label>
                                                    <span><input type="checkbox" name="remember"/>Remember Me</span>
                                                    <button class="default-big-btn" type="submit" value="Login">Login</button>
                                                    <button class="default-big-btn form-cancel" type="submit" value="">Cancel</button>
                                                </form>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="apply-btn-area">
                                                <a style="width: 160px;" href="{{route('registration')}}" class="apply-now-btn">Online Admission</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-menu-area bg-textPrimary" id="sticker">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-3">
                                <div class="logo-area">
                                    <a href="{{URL::to('/')}}"><img class="img-responsive" src="{{asset('public/upload/logos/'.$logo1->image)}}" alt="logo"></a>
                                </div>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10">
                                <nav id="desktop-nav">
                                    <ul>
                                        <li class="active">
                                            <a href="{{URL::to('/')}}">Home</a>
                                        </li>

                                        <li>
                                            <a href="{{route('about')}}">About US</a>
                                        </li>

                                        <li>
                                            <a href="{{route('lecturers')}}">lecturers</a>
                                        </li>

                                       <!--  <li>
                                            <a href="{{route('news')}}">News</a>
                                        </li>

                                        <li>
                                            <a href="{{route('event')}}">Event</a>
                                        </li> -->
        
                                        <!-- <li>
                                            <a href="{{route('gallery')}}">Gallery</a>
                                        </li> -->

                                        <li>
                                            <a href="{{route('faq')}}">Faq</a>
                                        </li>

                                        <li>
                                            <a href="{{route('contact')}}">Contact</a>
                                        </li>
                                        
                                    </ul>
                                </nav>
                            </div>

                           
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu Area Start -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                         <li class="active">
                                            <a href="{{URL::to('/')}}">Home</a>
                                        </li>

                                        <li>
                                            <a href="{{route('about')}}">About US</a>
                                        </li>

                                        <li>
                                            <a href="{{route('lecturers')}}">lecturers</a>
                                        </li>

                                        <!-- <li>
                                            <a href="{{route('news')}}">News</a>
                                        </li>

                                        <li>
                                            <a href="{{route('event')}}">Event</a>
                                        </li> -->
        
                                        <!-- <li>
                                            <a href="{{route('gallery')}}">Gallery</a>
                                        </li> -->

                                        <li>
                                            <a href="{{route('faq')}}">Faq</a>
                                        </li>
                                        
                                        <li>
                                            <a href="{{route('contact')}}">Contact</a>
                                        </li>

                                        <li>
                                            <a href="{{route('student.login')}}">Login</a>
                                        </li>

                                        <li>
                                            <a href="{{route('registration')}}">Online Admission</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu Area End -->
        </header>
        <!-- Header Area End Here -->


@yield('slider')

@yield('about_area')

@yield('featured_area')

@yield('lecturers_area')

@yield('video_area')

@yield('news_event_area')

@yield('student_say_area')

@yield('counter_area')



@yield('student_join_area')

@yield('brand_area')

@yield('lecturers_container')

@yield('about_content')

@yield('single_lecturers_content')

@yield('registration_content')

@yield('faq_content')

@yield('404_content')

@yield('news_content')

@yield('single_news_content')

@yield('event_content')

@yield('single_event_content')

@yield('gallery_content')

@yield('contact_content')













        <!-- Footer Area Start Here -->
        <footer>
            <div class="footer-area-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="footer-box">
                                <a href="{{URL::to('/')}}"><img class="img-responsive" src="{{asset('public/upload/logos/'.$logo2->image)}}" alt="logo"></a>
                                <div class="footer-about">
                                    <p>{{$text->text}}</p>
                                </div>
                                <ul class="footer-social">
                                    <li>
                                        <a target="_blank" href="{{$contactinfo->facebook}}">
                                            <i class="fa fa-facebook" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li><a target="_blank" href="{{$contactinfo->twitter}}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>

                                    <li><a target="_blank" href="{{$contactinfo->linkedin}}"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>

                                    <li><a target="_blank" href="{{$contactinfo->pinterest}}"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>

                                    <li><a target="_blank" href="{{$contactinfo->rss}}"><i class="fa fa-rss" aria-hidden="true"></i></a></li>

                                    <li><a target="_blank" href="{{$contactinfo->googleplus}}"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="footer-box">
                                <h3>Featured Links</h3>
                                <ul class="featured-links">
                                    <li>
                                        <ul>
                                            <li><a href="{{URL::to('/')}}">Home</a></li>
                                            <li><a href="{{route('about')}}">About Us</a></li>
                                        <li><a href="{{route('lecturers')}}">Lecturers</a></li>
                                            <li><a href="{{route('faq')}}">FAQs</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <ul>
                                        <!-- <li><a href="{{route('gallery')}}">Gallery</a></li> -->
                                        <li><a href="{{route('contact')}}">Contact Us</a></li>
                                            <li><a href="{{route('student.login')}}">Login</a></li>
                                            <li><a href="{{route('registration')}}">Registration</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                          
                               
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3618.7097256416946!2d89.3268994149419!3d24.907881084031548!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39fcff56efed3ab5%3A0x36f3982f09732ba5!2sMorakata%20Bazar!5e0!3m2!1sbn!2sbd!4v1618382164844!5m2!1sbn!2sbd" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

                        </div>
                     
                    </div>
                </div>
            </div>
            <div class="footer-area-bottom">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <p>&copy; 2021 P.S.D.C All Rights Reserved. Developed by<a target="_blank" href="http://softkhabbab.com" rel="nofollow"> Softkhabbab </a></p>
                        </div>
                        
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Area End Here -->
    </div>
    <!-- Main Body Area End Here -->
    <!-- jquery-->
    <script src="{{asset('public/frontend/js/jquery-2.2.4.min.js')}}" type="text/javascript"></script>
    <!-- Plugins js -->
    <script src="{{asset('public/frontend/js/plugins.js')}}" type="text/javascript"></script>
    <!-- Bootstrap js -->
    <script src="{{asset('public/frontend/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <!-- WOW JS -->
    <script src="{{asset('public/frontend/js/wow.min.js')}}"></script>
    <!-- Nivo slider js -->
    <script src="{{asset('public/frontend/vendor/slider/js/jquery.nivo.slider.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/frontend/vendor/slider/home.js')}}" type="text/javascript"></script>
    <!-- Owl Cauosel JS -->
    <script src="{{asset('public/frontend/vendor/OwlCarousel/owl.carousel.min.js')}}" type="text/javascript"></script>
    <!-- Meanmenu Js -->
    <script src="{{asset('public/frontend/js/jquery.meanmenu.min.js')}}" type="text/javascript"></script>
    <!-- Srollup js -->
    <script src="{{asset('public/frontend/js/jquery.scrollUp.min.js')}}" type="text/javascript"></script>
    <!-- jquery.counterup js -->
    <script src="{{asset('public/frontend/js/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('public/frontend/js/waypoints.min.js')}}"></script>
    <!-- Countdown js -->
    <script src="{{asset('public/frontend/js/jquery.countdown.min.js')}}" type="text/javascript"></script>
    <!-- Isotope js -->
    <script src="{{asset('public/frontend/js/isotope.pkgd.min.js')}}" type="text/javascript"></script>
    <!-- Magic Popup js -->
    <script src="{{asset('public/frontend/js/jquery.magnific-popup.min.js')}}" type="text/javascript"></script>
    <!-- Gridrotator js -->
    <script src="{{asset('public/frontend/js/jquery.gridrotator.js')}}" type="text/javascript"></script>
    <!-- Custom Js -->
    <script src="{{asset('public/frontend/js/main.js')}}" type="text/javascript"></script>

    <!-- jquery-validation -->
<script src="{{asset('public/backend')}}/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{asset('public/backend')}}/plugins/jquery-validation/additional-methods.min.js"></script>

<!-- toaster js -->
  <script src="{{asset('/public/frontend/toastr/toastr.min.js')}}"></script>

    <script>
      @if(Session::has('messagekey'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('messagekey') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('messagekey') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('messagekey') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('messagekey') }}");
            break;
    }
    @endif
  </script>

</body>
</html>
