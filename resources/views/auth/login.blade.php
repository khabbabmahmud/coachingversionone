<!DOCTYPE html>
<html>
<head>
    <title>Login Page</title>
   
<!-- Bootstrap CSS -->
 <link rel="stylesheet" href="{{asset('public/backend/login/bootstrap.min.css')}}">
    
<!-- Font-awesome CSS-->
    <link rel="stylesheet" href="{{asset('public/frontend/css/font-awesome.min.css')}}">

    <!-- jquery-->
    <script src="{{asset('public/frontend/js/jquery-2.2.4.min.js')}}" type="text/javascript"></script>

<!--   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->

    <!-- Bootstrap js -->
    <script src="{{asset('public/frontend/js/bootstrap.min.js')}}" type="text/javascript"></script>
    
    <style>

@import url('https://fonts.googleapis.com/css?family=Numans');

html,body{
background-image: url('{{'public/backend/login/login-background.jpg'}}');
background-size: cover;
background-repeat: no-repeat;
height: 100%;
font-family: 'Numans', sans-serif;
}

.container{
height: 100%;
align-content: center;
}

.card{
height: 370px;
margin-top: auto;
margin-bottom: auto;
width: 400px;
background-color: rgba(0,0,0,0.5) !important;
}

.social_icon span{
font-size: 60px;
margin-left: 10px;
color: #FFC312;
}

.social_icon span:hover{
color: white;
cursor: pointer;
}

.card-header h3{
color: white;
}

.social_icon{
position: absolute;
right: 20px;
top: -45px;
}

.input-group-prepend span{
width: 50px;
background-color: #FFC312;
color: black;
border:0 !important;
}

input:focus{
outline: 0 0 0 0  !important;
box-shadow: 0 0 0 0 !important;

}

.remember{
color: white;
}

.remember input
{
width: 20px;
height: 20px;
margin-left: 15px;
margin-right: 5px;
}

.login_btn{
color: black;
background-color: #FFC312;
width: 100px;
}

.login_btn:hover{
color: black;
background-color: white;
}

.links{
color: white;
}

.links a{
margin-left: 4px;
}
    </style>
</head>
<body>
<div class="container">
    <div class="d-flex justify-content-center h-100">
        <div class="card">
            <div class="card-header">
                <h3>Sign In</h3>
                <div class="d-flex justify-content-end social_icon">
                    <span><i class="fa fa-facebook-square"></i></span>
                    <span><i class="fa fa-google-plus-square"></i></span>
                    <span><i class="fa fa-twitter-square"></i></span>
                </div>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('login') }}">
                @csrf
                    @if($errors->any())
                    <div class="alert alert-danger alert-dismissible">
                         <button type="button" class="close" data-dismiss="alert">&times;</button>
                        @foreach($errors->all() as $error)
                         <strong>{{$error}}</strong><br>
                         @endforeach
                    </div>
                    @endif
                    
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-user"></i></span>
                        </div>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus>
                    </div>


                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-key"></i></span>
                        </div>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="current-password">
                    </div>


                    <!-- 
                    <div class="row align-items-center remember">
                        <input type="checkbox">Remember Me
                    </div> -->
                    

                    <div class="form-group">
                        <!-- <input type="submit" value="Login" class="btn float-right login_btn"> -->
                        <button type="submit" name="button" class="btn login_btn">Login</button>
                    </div>

                </form>
            </div>
           <!--  <div class="card-footer">
                <div class="d-flex justify-content-center links">
                    Don't have an account?<a href="#">Sign Up</a>
                </div>
                <div class="d-flex justify-content-center">
                    <a href="#">Forgot your password?</a>
                </div>
            </div> -->
        </div>
    </div>
</div>
</body>
</html>