@extends('welcome')
@section('contact_content')
<!-- Inner Page Banner Area Start Here -->
<div class="inner-page-banner-area" style="background-image: url('{{asset('public/frontend/img/banner/5.jpg')}}');">
    <div class="container">
        <div class="pagination-area">
            <h1>Contact Us</h1>
            <ul>
                <li><a href="#">Home</a> - </li>
                <li>Contact</li>
            </ul>
        </div>
    </div>
</div>
<!-- Inner Page Banner Area End Here -->
<!-- Contact Us Page 1 Area Start Here -->
<div class="contact-us-page1-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="contact-us-info1">
                    <ul>
                        <li>
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <h3>Phone</h3>
                            <p>{{$contactinfo->number}}</p>
                        </li>
                        <li>
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <h3>Address</h3>
                            <p>{{$contactinfo->address}}</p>
                        </li>
                        <li>
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            <h3>E-mail</h3>
                            <p>{{$contactinfo->email}}</p>
                        </li>
                        <li>
                            <h3>Follow Us</h3>
                            <ul class="contact-social">
                                <li>
                                    <a target="_blank" href="{{$contactinfo->facebook}}"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                </li>

                                <li>
                                    <a target="_blank" href="{{$contactinfo->twitter}}"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                </li>

                                <li>
                                    <a target="_blank" href="{{$contactinfo->linkedin}}"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                </li>

                                <li>
                                    <a target="_blank" href="{{$contactinfo->pinterest}}"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                                </li>

                                <li>
                                    <a target="_blank" href="{{$contactinfo->rss}}"><i class="fa fa-rss" aria-hidden="true"></i></a>
                                </li>

                                <li>
                                    <a target="_blank" href="{{$contactinfo->googleplus}}"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2 class="title-default-left title-bar-high">Contact With Us</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="contact-form1">
                <form method="post" action="{{route('contact.store')}}" id="myForm">
                @csrf
                            <fieldset>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" placeholder="Name*" class="form-control" name="name">
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="email" placeholder="Email*" class="form-control" name="email">
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" placeholder="Phone Number*" class="form-control" name="phone">
                                        
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" placeholder="Address*" class="form-control" name="address">
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <textarea placeholder="Message*" class="textarea form-control" name="msg" rows="8" cols="20" ></textarea>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-6 col-sm-12">
                                    <div class="form-group margin-bottom-none">
                                        <button type="submit" class="default-big-btn">Send Message</button>
                                    </div>
                                </div>
                                
                                <div class="col-lg-8 col-md-8 col-sm-6 col-sm-12">
                                    <div class='form-response'></div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="google-map-area">
                <div id="googleMap" style="width:100%; height:395px;"></div>
            </div>
        </div>
    </div>
</div>
<!-- Contact Us Page 1 Area End Here -->

<script>
$(function () {
  $('#myForm').validate({
    rules: {
      name: {
        required: true,
      },
      email: {
        required: true,
        email: true,
      },
      phone: {
        required: true,
      },
      address: {
        required: true,
      },
      msg: {
        required: true,
      },
      
    },
    messages: {
      
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>

@endsection