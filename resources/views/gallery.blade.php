@extends('welcome')

@section('gallery_content')

       <!-- Inner Page Banner Area Start Here -->
        <div class="inner-page-banner-area" style="background-image: url('{{asset('public/frontend/img/banner/5.jpg')}}');">
            <div class="container">
                <div class="pagination-area">
                    <h1>Gallery</h1>
                    <ul>
                        <li><a href="#">Home</a> -</li>
                        <li>Gallery</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Inner Page Banner Area End Here -->
        <!-- Gallery Area 2 Start Here -->
        <div class="gallery-area2">
            <div class="container" id="inner-isotope">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="isotop-classes-tab isotop-btn">
                            <a href="#" data-filter="*">All</a>
                            <a href="#" data-filter=".library"> Library</a>
                            <a href="#" data-filter=".room" class="current">Room</a>
                            <a href="#" data-filter=".auditoriam">Auditoriam</a>
                            <a href="#" data-filter=".campus">Campus</a>
                            <a href="#" data-filter=".class">Class</a>
                        </div>
                    </div>
                </div>
                <div class="row featuredContainer gallery-wrapper">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 library auditoriam">
                        <div class="gallery-box">
                            <img src="{{asset('public/frontend/img/gallery/6.jpg')}}" class="img-responsive" alt="gallery">
                            <div class="gallery-content">
                                <a href="{{asset('public/frontend/img/gallery/6.jpg')}}" class="zoom"><i class="fa fa-link" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 room auditoriam campus">
                        <div class="gallery-box">
                            <img src="{{asset('public/frontend/img/gallery/7.jpg')}}" class="img-responsive" alt="gallery">
                            <div class="gallery-content">
                                <a href="{{asset('public/frontend/img/gallery/7.jpg')}}" class="zoom"><i class="fa fa-link" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 library room campus">
                        <div class="gallery-box">
                            <img src="{{asset('public/frontend/img/gallery/8.jpg')}}" class="img-responsive" alt="gallery">
                            <div class="gallery-content">
                                <a href="{{asset('public/frontend/img/gallery/8.jpg')}}" class="zoom"><i class="fa fa-link" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 library auditoriam">
                        <div class="gallery-box">
                            <img src="{{asset('public/frontend/img/gallery/9.jpg')}}" class="img-responsive" alt="gallery">
                            <div class="gallery-content">
                                <a href="{{asset('public/frontend/img/gallery/9.jpg')}}" class="zoom"><i class="fa fa-link" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 room auditoriam class">
                        <div class="gallery-box">
                            <img src="{{asset('public/frontend/img/gallery/10.jpg')}}" class="img-responsive" alt="gallery">
                            <div class="gallery-content">
                                <a href="{{asset('public/frontend/img/gallery/10.jpg')}}" class="zoom"><i class="fa fa-link" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 library campus class">
                        <div class="gallery-box">
                            <img src="{{asset('public/frontend/img/gallery/11.jpg')}}" class="img-responsive" alt="gallery">
                            <div class="gallery-content">
                                <a href="{{asset('public/frontend/img/gallery/11.jpg')}}" class="zoom"><i class="fa fa-link" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 room auditoriam campus">
                        <div class="gallery-box">
                            <img src="{{asset('public/frontend/img/gallery/12.jpg')}}" class="img-responsive" alt="gallery">
                            <div class="gallery-content">
                                <a href="{{asset('public/frontend/img/gallery/12.jpg')}}" class="zoom"><i class="fa fa-link" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 library room auditoriam">
                        <div class="gallery-box">
                            <img src="{{asset('public/frontend/img/gallery/13.jpg')}}" class="img-responsive" alt="gallery">
                            <div class="gallery-content">
                                <a href="{{asset('public/frontend/img/gallery/13.jpg')}}" class="zoom"><i class="fa fa-link" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 auditoriam campus">
                        <div class="gallery-box">
                            <img src="{{asset('public/frontend/img/gallery/14.jpg')}}" class="img-responsive" alt="gallery">
                            <div class="gallery-content">
                                <a href="img/gallery/14.jpg" class="zoom"><i class="fa fa-link" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 library room auditoriam">
                        <div class="gallery-box">
                            <img src="{{asset('public/frontend/img/gallery/15.jpg')}}" class="img-responsive" alt="gallery">
                            <div class="gallery-content">
                                <a href="{{asset('public/frontend/img/gallery/15.jpg')}}" class="zoom"><i class="fa fa-link" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 library campus class">
                        <div class="gallery-box">
                            <img src="{{asset('public/frontend/img/gallery/16.jpg')}}" class="img-responsive" alt="gallery">
                            <div class="gallery-content">
                                <a href="{{asset('public/frontend/img/gallery/16.jpg')}}" class="zoom"><i class="fa fa-link" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 library room auditoriam campus">
                        <div class="gallery-box">
                            <img src="{{asset('public/frontend/img/gallery/17.jpg')}}" class="img-responsive" alt="gallery">
                            <div class="gallery-content">
                                <a href="{{asset('public/frontend/img/gallery/17.jpg')}}" class="zoom"><i class="fa fa-link" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Gallery Area 2 End Here -->

@endsection