@extends('welcome')
@section('about_content')
<!-- Inner Page Banner Area Start Here -->
<div class="inner-page-banner-area" style="background-image: url('{{asset('public/frontend/img/banner/5.jpg')}}');">
    <div class="container">
        <div class="pagination-area">
            <h1>About us</h1>
            <ul>
                <li><a href="#">Home</a> - </li>
                <li>About</li>
            </ul>
        </div>
    </div>
</div>
<!-- Inner Page Banner Area End Here -->

<!-- About Slider Area Start Here -->
<div class="about-slider-area">

    <div class="container">
        <div class="slider1-area">
            <div class="bend niceties preview-1">
                <div id="ensign-nivoslider-3" class="slides">
                    @foreach($sliders as $slider)
                    <img src="{{asset('public/upload/sliders/'.$slider->image)}}" alt="slider" title="#slider-direction-1" />

                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <h2 class="title-center">We Have A Friendly & Familiar Campus For You</h2>
        <p class="sub-title-full-width">Bmply dummy text of the printing and typesetting indust Lorem Ipsum has been theitry's snce simply dummy text of the printing.Phasellus enim libero, blandit vel sapien vita their.Lorem ipsum dolor sit amet, consectetur adipiscino eiusmod tempor incididply dummy text of the printing and typesetting indust Lorem Ipsum has been theitry's snce simply dummy text of the printing.</p>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="featured-box2 hvr-bounce-to-right">
                    <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                    <h3><a href="#">Scholarship Facility</a></h3>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="featured-box2 hvr-bounce-to-right">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <h3><a href="#">100 Skilled Lecturers</a></h3>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="featured-box2 hvr-bounce-to-right">
                    <i class="fa fa-bed" aria-hidden="true"></i>
                    <h3><a href="#">Hostel Facilities</a></h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About Slider Area End Here -->

<!-- Counter Area Start Here -->
<div class="counter-area bg-primary-deep" style="background-image: url('{{asset('public/frontend/img/banner/4.jpg')}}');">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 counter1-box wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".20s">
                <h2 class="about-counter title-bar-counter" data-num="{{$counter->one}}">{{$counter->one}}</h2>
                <p>{{$counter->one_title}}</p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 counter1-box wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".40s">
                <h2 class="about-counter title-bar-counter" data-num="{{$counter->two}}">{{$counter->two}}</h2>
                <p>{{$counter->two_title}}</p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 counter1-box wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".60s">
                <h2 class="about-counter title-bar-counter" data-num="{{$counter->three}}">{{$counter->three}}</h2>
                <p>{{$counter->three_title}}</p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 counter1-box wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".80s">
                <h2 class="about-counter title-bar-counter" data-num="{{$counter->four}}">{{$counter->four}}</h2>
                <p>{{$counter->four_title}}</p>
            </div>

        </div>
    </div>
</div>
<!-- Counter Area End Here -->

<!-- Lecturers Area Start Here -->
<div class="lecturers-area">
    <div class="container">
        <h2 class="title-default-left">Our Skilled Lecturers</h2>
    </div>
    <div class="container">
        <div class="rc-carousel" data-loop="true" data-items="4" data-margin="30" data-autoplay="true" data-autoplay-timeout="10000" data-smart-speed="2000" data-dots="false" data-nav="true" data-nav-speed="false" data-r-x-small="1" data-r-x-small-nav="true" data-r-x-small-dots="false" data-r-x-medium="2" data-r-x-medium-nav="true" data-r-x-medium-dots="false" data-r-small="3" data-r-small-nav="true" data-r-small-dots="false" data-r-medium="4" data-r-medium-nav="true" data-r-medium-dots="false" data-r-large="4" data-r-large-nav="true" data-r-large-dots="false">
        
        @foreach($lacturars as $lacturar)
            <div class="single-item">
                <div class="single-item-wrapper">

                    <div class="lecturers-img-wrapper">
                        <a href="{{route('single_lecturer',$lacturar->id)}}">
                            <img class="img-responsive" src="{{asset('public/upload/user-images/'.$lacturar->image)}}" alt="team">
                        </a>
                    </div>

                    <div class="lecturers-content-wrapper">

                        <h3 class="item-title">
                            <a href="{{route('single_lecturer',$lacturar->id)}}">{{$lacturar->name}}</a>
                        </h3>

                        <span class="item-designation">{{$lacturar->title}}</span>

                        <ul class="lecturers-social">
                          
                            <li><a target="_blank" href="{{$lacturar->li}}"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>

                            <li><a target="_blank" href="{{$lacturar->tw}}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>

                            <li><a target="_blank" href="{{$lacturar->fb}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            
                        </ul>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<!-- Lecturers Area End Here -->

@endsection