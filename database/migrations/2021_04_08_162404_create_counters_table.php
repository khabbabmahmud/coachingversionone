<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('one')->nullable();
            $table->string('one_title')->nullable();
            $table->integer('two')->nullable();
            $table->string('two_title')->nullable();
            $table->integer('three')->nullable();
            $table->string('three_title')->nullable();
            $table->integer('four')->nullable();
            $table->string('four_title')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counters');
    }
}
