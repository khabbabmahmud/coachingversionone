<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWelcometosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('welcometos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('short_title')->nullable();
            $table->string('long_title')->nullable();
            $table->string('sone_title')->nullable();
            $table->string('sone_subtitle')->nullable();
            $table->string('stwo_title')->nullable();
            $table->string('stwo_subtitle')->nullable();
            $table->string('sthree_title')->nullable();
            $table->string('sthree_subtitle')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('welcometos');
    }
}
