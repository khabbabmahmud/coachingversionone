<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id')->nullable();
            $table->string('usertype')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->unique();
            $table->string('address')->nullable();
            $table->string('mobile')->nullable();
            $table->string('gender')->nullable();
            $table->string('image')->nullable();
            $table->string('password')->nullable();
            $table->string('title')->nullable();
            $table->string('fb')->nullable();
            $table->string('tw')->nullable();
            $table->string('li')->nullable();
            $table->string('about')->nullable();
            $table->string('q1')->nullable();
            $table->string('q2')->nullable();
            $table->string('q3')->nullable();
            $table->string('q4')->nullable();
            $table->string('q5')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
