<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

// Route::get('/clear-cache', function(){
// 	$exitcode = Artisan::call('cache:clear');

// 	if ($exitcode) {
// 		$notification = array(
// 		'messagekey'        => 'Logo Not Updated ',
// 		'alert-type'    => 'info'
// 		 ); return redirect()->back()->with($notification); 
// 	}

// });

Route::get('/', 'frontend@index');

Route::get('lecturers', 'frontend@lecturers')->name('lecturers');

Route::get('about-us', 'frontend@about')->name('about');

Route::get('lecturers-details-{id}', 'frontend@singleLecturer')->name('single_lecturer');

Route::get('registration', 'frontend@registration')->name('registration');

Route::get('faq', 'frontend@faq')->name('faq');

Route::get('404', 'frontend@error_page')->name('404');

Route::get('news', 'frontend@news')->name('news');

Route::get('single-news', 'frontend@single_news')->name('single_news');

Route::get('event', 'frontend@event')->name('event');

Route::get('single_event', 'frontend@single_event')->name('single_event');

Route::get('gallery', 'frontend@gallery')->name('gallery');

Route::get('contact', 'frontend@contact')->name('contact');
Route::post('contact/store', 'frontend@communicateStore')->name('contact.store');

Route::get('student-login', 'frontend@studentLogin')->name('student.login');



//=========Website backend section start here =========//
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// ========Auth Middleware section Start here===========//

Route::group(['middleware' => 'auth'], function(){

//============ user management start here =========//
Route::prefix('users')->group(function(){
	Route::get('/view','Backend\UsersController@view')->name('users.view');
	Route::get('/add','Backend\UsersController@add')->name('users.add');
	Route::post('/store','Backend\UsersController@store')->name('users.store');
	Route::get('/edit/{id}','Backend\UsersController@edit')->name('user.edit');
	Route::post('/update/{id}','Backend\UsersController@update')->name('users.update');
	Route::get('/delete/{id}','Backend\UsersController@delete')->name('user.delete');
});
//============ user management end here =========//



//============ Profile management start here =========//
Route::prefix('profiles')->group(function(){
	Route::get('/view','Backend\ProfileController@view')->name('profiles.view');
	Route::get('/edit','Backend\ProfileController@edit')->name('profiles.edit');
	Route::post('/update','Backend\ProfileController@update')->name('profiles.update');
	Route::get('/password/view','Backend\ProfileController@passwordView')->name('profiles.password.view');
	Route::post('/password/update','Backend\ProfileController@passwordUpdate')->name('profiles.password.update');
});
//============ Profile management end here =========//

//============ contact information management start here =========//
Route::prefix('contactinfo')->group(function(){
	Route::get('/view','Backend\ContactinfoController@view')->name('contactinfo.view');
	Route::post('/update/{id}','Backend\ContactinfoController@update')->name('contactinfo.update');
});
//============ contact information management end here =========//

//============ communicate management start here =========//
Route::prefix('communicate')->group(function(){
	Route::get('/view','Backend\Communicatecontroller@view')->name('communicate.view');
	Route::get('/delete/{id}','Backend\Communicatecontroller@delete')->name('communicate.delete');
});
//============ communicate management end here =========//


//============ logo management start here =========//
Route::prefix('logos')->group(function(){
	Route::get('/view','Backend\LogoController@view')->name('logos.view');
	Route::get('/edit/{id}','Backend\LogoController@edit')->name('logos.edit');
	Route::post('/update/{id}','Backend\LogoController@update')->name('logos.update');
});
//============ logo management end here =========//


//============ sliders management start here =========//
Route::prefix('sliders')->group(function(){
	Route::get('/view','Backend\SliderController@view')->name('sliders.view');
	Route::get('/add','Backend\SliderController@add')->name('sliders.add');
	Route::post('/store','Backend\SliderController@store')->name('sliders.store');
	Route::get('/edit/{id}','Backend\SliderController@edit')->name('sliders.edit');
	Route::post('/update/{id}','Backend\SliderController@update')->name('sliders.update');
	Route::get('/delete/{id}','Backend\SliderController@delete')->name('sliders.delete');
});
//============ sliders management end here =========//

//============ welcome management start here =========//
Route::prefix('welcome')->group(function(){
	Route::get('/view','Backend\WelcomeController@view')->name('welcome.view');
	Route::get('/edit/{id}','Backend\WelcomeController@edit')->name('welcome.edit');
	Route::post('/update/{id}','Backend\WelcomeController@update')->name('welcome.update');
});
//============ welcome management end here =========//

//============ course management start here =========//
Route::prefix('course')->group(function(){
	Route::get('/view','Backend\CourseController@view')->name('course.view');
	Route::get('/add','Backend\CourseController@add')->name('course.add');
	Route::post('/store','Backend\CourseController@store')->name('course.store');
	Route::get('/edit/{id}','Backend\CourseController@edit')->name('course.edit');
	Route::post('/update/{id}','Backend\CourseController@update')->name('course.update');
	Route::get('/delete/{id}','Backend\CourseController@delete')->name('course.delete');
});
//============ course management end here =========//

//============ Watch Campus management start here =========//
Route::prefix('watch')->group(function(){
	Route::get('/view','Backend\WatchController@view')->name('watch.view');
	Route::get('/edit/{id}','Backend\WatchController@edit')->name('watch.edit');
	Route::post('/update/{id}','Backend\WatchController@update')->name('watch.update');
});
//============ Watch Campus management end here =========//

//============ counter management start here =========//
Route::prefix('counter')->group(function(){
	Route::get('/view','Backend\CounterController@view')->name('counter.view');
	Route::get('/edit/{id}','Backend\CounterController@edit')->name('counter.edit');
	Route::post('/update/{id}','Backend\CounterController@update')->name('counter.update');
});
//============ counter management end here =========//

//============ Studentsay  start here =========//
Route::prefix('studentsay')->group(function(){
	Route::get('/view','Backend\StudentsayController@view')->name('studentsay.view');
	Route::get('/add','Backend\StudentsayController@add')->name('studentsay.add');
	Route::post('/store','Backend\StudentsayController@store')->name('studentsay.store');
	Route::get('/edit/{id}','Backend\StudentsayController@edit')->name('studentsay.edit');
	Route::post('/update/{id}','Backend\StudentsayController@update')->name('studentsay.update');
	Route::get('/delete/{id}','Backend\StudentsayController@delete')->name('studentsay.delete');
});
//============ Studentsay  end here =========//

//============ partner  start here =========//
Route::prefix('partner')->group(function(){
	Route::get('/view','Backend\PartnerController@view')->name('partner.view');
	Route::get('/add','Backend\PartnerController@add')->name('partner.add');
	Route::post('/store','Backend\PartnerController@store')->name('partner.store');
	Route::get('/delete/{id}','Backend\PartnerController@delete')->name('partner.delete');
});
//============ partner  end here =========//

//============ footertext information management start here =========//
Route::prefix('footertext')->group(function(){
	Route::get('/view','Backend\footerController@view')->name('footertext.view');
	Route::post('/update/{id}','Backend\footerController@update')->name('footertext.update');
});
//============ contact information management end here =========//


//============ FAQ management start here =========//
Route::prefix('faq')->group(function(){
	Route::get('/view','Backend\FaQController@view')->name('faq.view');
	Route::get('/add','Backend\FaQController@add')->name('faq.add');
	Route::post('/store','Backend\FaQController@store')->name('faq.store');
	Route::get('/edit/{id}','Backend\FaQController@edit')->name('faq.edit');
	Route::post('/update/{id}','Backend\FaQController@update')->name('faq.update');
	Route::get('/delete/{id}','Backend\FaQController@delete')->name('faq.delete');
});
//============ FAQ management end here =========//



//============ Setup management start here =========//
Route::prefix('setups')->group(function(){
	// class
	Route::get('/student/class/view','Backend\StudentClassController@view')->name('setups.studentclass.view');

	Route::get('/student/class/add','Backend\StudentClassController@add')->name('setups.studentclass.add');

	Route::post('/student/class/store','Backend\StudentClassController@store')->name('setups.studentclass.store');

	Route::get('/student/class/edit/{id}','Backend\StudentClassController@edit')->name('setups.studentclass.edit');

	Route::post('/student/class/update/{id}','Backend\StudentClassController@update')->name('setups.studentclass.update');
	
	Route::get('/student/class/delete/{id}','Backend\StudentClassController@delete')->name('setups.studentclass.delete');
	
	// Year / sessions
	Route::get('/student/year/view','Backend\StudentYearController@view')->name('setups.studentyear.view');

	Route::get('/student/year/add','Backend\StudentYearController@add')->name('setups.studentyear.add');

	Route::post('/student/year/store','Backend\StudentYearController@store')->name('setups.studentyear.store');

	Route::get('/student/year/edit/{id}','Backend\StudentYearController@edit')->name('setups.studentyear.edit');

	Route::post('/student/year/update/{id}','Backend\StudentYearController@update')->name('setups.studentyear.update');
	
	Route::get('/student/year/delete/{id}','Backend\StudentYearController@delete')->name('setups.studentyear.delete');

	// Group / Sections
	Route::get('/student/group/view','Backend\StudentGroupController@view')->name('setups.studentgroup.view');

	Route::get('/student/group/add','Backend\StudentGroupController@add')->name('setups.studentgroup.add');

	Route::post('/student/group/store','Backend\StudentGroupController@store')->name('setups.studentgroup.store');

	Route::get('/student/group/edit/{id}','Backend\StudentGroupController@edit')->name('setups.studentgroup.edit');

	Route::post('/student/group/update/{id}','Backend\StudentGroupController@update')->name('setups.studentgroup.update');
	
	Route::get('/student/group/delete/{id}','Backend\StudentGroupController@delete')->name('setups.studentgroup.delete');

	// Shift
	Route::get('/student/shift/view','Backend\StudentShiftController@view')->name('setups.studentshift.view');

	Route::get('/student/shift/add','Backend\StudentShiftController@add')->name('setups.studentshift.add');

	Route::post('/student/shift/store','Backend\StudentShiftController@store')->name('setups.studentshift.store');

	Route::get('/student/shift/edit/{id}','Backend\StudentShiftController@edit')->name('setups.studentshift.edit');

	Route::post('/student/shift/update/{id}','Backend\StudentShiftController@update')->name('setups.studentshift.update');
	
	Route::get('/student/shift/delete/{id}','Backend\StudentShiftController@delete')->name('setups.studentshift.delete');

	// Fee Category
	Route::get('/fee/category/view','Backend\FeeCategoryController@view')->name('setups.studentfeecategory.view');

	Route::get('/fee/category/add','Backend\FeeCategoryController@add')->name('setups.studentfeecategory.add');

	Route::post('/fee/category/store','Backend\FeeCategoryController@store')->name('setups.studentfeecategory.store');

	Route::get('/fee/category/edit/{id}','Backend\FeeCategoryController@edit')->name('setups.studentfeecategory.edit');

	Route::post('/fee/category/update/{id}','Backend\FeeCategoryController@update')->name('setups.studentfeecategory.update');
	
	Route::get('/fee/category/delete/{id}','Backend\FeeCategoryController@delete')->name('setups.studentfeecategory.delete');
	// Fee Amoun
	Route::get('/fee/amount/view','Backend\FeeAmountController@view')->name('setups.studentfeeamount.view');

	Route::get('/fee/amount/add','Backend\FeeAmountController@add')->name('setups.studentfeeamount.add');

	Route::post('/fee/amount/store','Backend\FeeAmountController@store')->name('setups.studentfeeamount.store');

	Route::get('/fee/amount/details/{fee_category_id}','Backend\FeeAmountController@details')->name('setups.studentfeeamount.details');

	Route::get('/fee/amount/edit/{fee_category_id}','Backend\FeeAmountController@edit')->name('setups.studentfeeamount.edit');

	Route::post('/fee/amount/update/{fee_category_id}','Backend\FeeAmountController@update')->name('setups.studentfeeamount.update');
	
	Route::get('/fee/amount/delete/{fee_category_id}','Backend\FeeAmountController@delete')->name('setups.studentfeeamount.delete');

	// Exam Type
	Route::get('/exam/type/view','Backend\ExamTypeController@view')->name('setups.examtype.view');

	Route::get('/exam/type/add','Backend\ExamTypeController@add')->name('setups.examtype.add');

	Route::post('/exam/type/store','Backend\ExamTypeController@store')->name('setups.examtype.store');

	Route::get('/exam/type/edit/{id}','Backend\ExamTypeController@edit')->name('setups.examtype.edit');

	Route::post('/exam/type/update/{id}','Backend\ExamTypeController@update')->name('setups.examtype.update');


	// Subject
	Route::get('/subject/view','Backend\SubjectController@view')->name('setups.subject.view');

	Route::get('/subject/add','Backend\SubjectController@add')->name('setups.subject.add');

	Route::post('/subject/store','Backend\SubjectController@store')->name('setups.subject.store');

	Route::get('/subject/edit/{id}','Backend\SubjectController@edit')->name('setups.subject.edit');

	Route::post('/subject/update/{id}','Backend\SubjectController@update')->name('setups.subject.update');







});
//============ Setup management end here =========//




});
// ========Auth Middleware section End here===========//
//=========Website backend section End here =========//