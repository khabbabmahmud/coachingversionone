<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Studentshift;

class StudentShiftController extends Controller
{
    public function view(){
   	$data = Studentshift::all();
   	return view('backend.setup.shift.view-shift', compact('data'));
   }

   public function add(){
   		return view('backend.setup.shift.add-shift');
   }

   public function store(Request $request){
   	$request->validate([
   		'name'		=> 'required|unique:years,name',
   	]);
   	$data = new Studentshift();
   	$data->name = $request->name;
	$save = $data->save();
	   	if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Shift Inserted',
                'alert-type'    => 'success'
            );
	   	}return redirect()->route('setups.studentshift.view')->with($notification);
   }

   public function edit($id){
   	$editData = Studentshift::find($id);
   	return view('backend.setup.shift.add-shift',compact('editData'));
   }

   public function update(Request $request,$id){
		$data = Studentshift::find($id);
		$request->validate([
   		'name'		=> 'required|unique:years,name',
   		]);
   	$data->name = $request->name;
	$save = $data->save();
	   	if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Shift Updated',
                'alert-type'    => 'success'
            );
	   	}return redirect()->route('setups.studentshift.view')->with($notification);
   }
}
