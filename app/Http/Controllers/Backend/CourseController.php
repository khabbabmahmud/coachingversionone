<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use App\Model\Course;

class CourseController extends Controller
{
    public function view(){
   	$data = Course::orderBy('id', 'desc')->get();
   	return view('backend.course.view-course', compact('data'));
   }
   public function add(){
   		return view('backend.course.add-course');
   }
   public function store(Request $request){
   	$data = new Course();

   	$data->short_title = $request->short_title;
   	$data->long_title = $request->long_title;
   	$data->duration = $request->duration;
   	$data->classess = $request->classess;
   	$data->class_time = $request->class_time;
   	
	if ($request->file('image')) {
		$file = $request->file('image');
		$filename = date('YmdHi').$file->getClientOriginalName();
		$file->move(public_path('upload/course'),$filename);
		$data->image= $filename;
	}
	$save = $data->save();
	   	if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Course Inserted',
                'alert-type'    => 'success'
            );
	   	}
	   	return redirect()->route('course.view')->with($notification);

   }

   public function edit($id){
   	$editData = Course::find($id);
   	return view('backend.course.edit-course',compact('editData'));
   }

   public function update(Request $request,$id){
		$data = Course::find($id);
		$data->short_title = $request->short_title;
	   	$data->long_title = $request->long_title;
	   	$data->duration = $request->duration;
	   	$data->classess = $request->classess;
	   	$data->class_time = $request->class_time;

		if ($request->file('image')) {
			$file = $request->file('image');
			@unlink(public_path('upload/course/'.$data->image));
			$filename = date('YmdHi').$file->getClientOriginalName();
			$file->move(public_path('upload/course'),$filename);
			$data->image= $filename;
		}
		$save = $data->save();
			if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Course Updated ',
                'alert-type'    => 'success'
            );
	   	}
	   	return redirect()->route('course.view')->with($notification);
   }

   public function delete($id){
   	$data = Course::find($id);
   	if (file_exists('public/upload/course/'.$data->image) AND ! empty($data->image)) {
   		unlink('public/upload/course/'.$data->image);
   	}
   	$data->delete();
		$notification = array(
    	'messagekey'        => 'Successfully Course Deleted ',
    	'alert-type'    => 'info'
       );
	   	return redirect()->route('course.view')->with($notification);
   }




}
