<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Subject;


class SubjectController extends Controller
{
        public function view(){
   	$data = Subject::all();
   	return view('backend.setup.subject.view-subject', compact('data'));
   }

   public function add(){
   		return view('backend.setup.subject.add-subject');
   }
   public function store(Request $request){
   	$request->validate([
   		'name'		=> 'required|unique:subjects,name',
   	]);
   	$data = new Subject();
   	$data->name = $request->name;
	$save = $data->save();
	   	if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Subject Inserted',
                'alert-type'    => 'success'
            );
	   	}return redirect()->route('setups.subject.view')->with($notification);
   }

   public function edit($id){
   	$editData = Subject::find($id);
   	return view('backend.setup.subject.add-subject',compact('editData'));
   }

   public function update(Request $request,$id){
		$data = Subject::find($id);
		$request->validate([
   		'name'		=> 'required|unique:subjects,name',
   		]);
   	$data->name = $request->name;
	$save = $data->save();
	   	if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Subject Updated',
                'alert-type'    => 'success'
            );
	   	}return redirect()->route('setups.subject.view')->with($notification);
   }
}
