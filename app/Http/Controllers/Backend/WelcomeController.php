<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Model\Welcometo;

class WelcomeController extends Controller
{
    public function view(){
   	$data = Welcometo::all();
   	return view('backend.welcometo.view-welcometo', compact('data'));
   }
   public function edit($id){
   	$editData = Welcometo::find($id);
   	return view('backend.welcometo.edit-welcometo',compact('editData'));
   }

    public function update(Request $request,$id){
    $data = Welcometo::find($id);
    
    $data->short_title = $request->short_title;
    $data->long_title = $request->long_title;
    $data->sone_title = $request->sone_title;
    $data->sone_subtitle = $request->sone_subtitle;
    $data->stwo_title = $request->stwo_title;
    $data->stwo_subtitle = $request->stwo_subtitle;
    $data->sthree_title = $request->sthree_title;
    $data->sthree_subtitle = $request->sthree_subtitle;
    $data->created_by = $request->created_by;
    $data->updated_by = $request->updated_by;
    
    $save = $data->save();
      if ($save) {
          $notification = array(
                'messagekey'        => 'Successfully Slider Updated ',
                'alert-type'    => 'success'
            );
      }
      return redirect()->route('welcome.view')->with($notification);
   }





}
