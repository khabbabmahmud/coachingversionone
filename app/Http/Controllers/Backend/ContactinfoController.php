<?php


namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Model\Contactinfo;
use DB;

class ContactinfoController extends Controller
{

    public function view(){
    	
     // $logo1 = Logo::where('id', 1)->first();
     //  $logo2 = Logo::where('id', 2)->first();

   	$data = DB::table('contactinfos')->where('id', 1)->first();
   	return view('backend.contact-information.view-contactinfo', compact('data'));
   }

      public function update(Request $request,$id){

		$data = Contactinfo::find($id);
		$data->number = $request->number; 
		$data->email = $request->email; 
		$data->address = $request->address; 
		$data->facebook = $request->facebook; 
		$data->twitter = $request->twitter; 
		$data->linkedin = $request->linkedin; 
		$data->pinterest = $request->pinterest; 
		$data->rss = $request->rss; 
		$data->googleplus = $request->googleplus; 

		$save = $data->save();
			if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Contact Info Updated ',
                'alert-type'    => 'success'
            );
	   	}
	   	return redirect()->route('contactinfo.view')->with($notification);

		
	   			
   }
}
