<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Year;


class StudentYearController extends Controller
{
    public function view(){
   	$data = Year::all();
   	return view('backend.setup.year.view-year', compact('data'));
   }

   public function add(){
   		return view('backend.setup.year.add-year');
   }
   public function store(Request $request){
   	$request->validate([
   		'name'		=> 'required|unique:years,name',
   	]);
   	$data = new Year();
   	$data->name = $request->name;
	$save = $data->save();
	   	if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Year Inserted',
                'alert-type'    => 'success'
            );
	   	}return redirect()->route('setups.studentyear.view')->with($notification);
   }

   public function edit($id){
   	$editData = Year::find($id);
   	return view('backend.setup.year.add-year',compact('editData'));
   }

   public function update(Request $request,$id){
		$data = Year::find($id);
		$request->validate([
   		'name'		=> 'required|unique:years,name',
   		]);
   	$data->name = $request->name;
	$save = $data->save();
	   	if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Year Updated',
                'alert-type'    => 'success'
            );
	   	}return redirect()->route('setups.studentyear.view')->with($notification);
   }

}
