<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Partner;

class PartnerController extends Controller
{
    public function view(){
   	$data = Partner::orderBy('id', 'desc')->get();
   	return view('backend.partner.view-partner', compact('data'));
   }
   public function add(){
   		return view('backend.partner.add-partner');
   }
   public function store(Request $request){
   	$data = new Partner();
	if ($request->file('image')) {
		$file = $request->file('image');
		$filename = date('YmdHi').$file->getClientOriginalName();
		$file->move(public_path('upload/partner'),$filename);
		$data->image= $filename;
	}
	$save = $data->save();
	   	if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Partner Inserted',
                'alert-type'    => 'success'
            );
	   	}	return redirect()->route('partner.view')->with($notification);

   }
   public function delete($id){
   	$data = Partner::find($id);
   	if (file_exists('public/upload/partner/'.$data->image) AND ! empty($data->image)) {
   		unlink('public/upload/partner/'.$data->image);
   	}
   	$data->delete();
		$notification = array(
    	'messagekey'        => 'Successfully Partner Deleted',
    	'alert-type'    => 'info'
       );return redirect()->route('partner.view')->with($notification);
   }
}
