<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Model\Logo;

class LogoController extends Controller
{
    public function view(){
   	$data = Logo::all();
   	return view('backend.logo.view-logo', compact('data'));
   }

   public function edit($id){
   	$editData = Logo::find($id);
   	return view('backend.logo.edit-logo', compact('editData'));
   }

   public function update(Request $request,$id){
		$data = Logo::find($id);
	
		if ($request->file('image')) {
			$file = $request->file('image');
			@unlink(public_path('upload/logos/'.$data->image));
			$filename = date('YmdHi').$file->getClientOriginalName();
			$file->move(public_path('upload/logos'),$filename);
			$data['image'] = $filename;
			$save = $data->save();
			if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Logo Updated ',
                'alert-type'    => 'success'
            );
	   	}
	   	return redirect()->route('logos.view')->with($notification);
		}else{
			$notification = array(
			                'messagekey'        => 'Logo Not Updated ',
			                'alert-type'    => 'info'
			            ); return redirect()->route('logos.view')->with($notification);
		}
	   			
   }

   

}
