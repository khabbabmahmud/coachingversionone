<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Feecategory;

class FeeCategoryController extends Controller
{
    public function view(){
   	$data = Feecategory::all();
   	return view('backend.setup.fee_category.view-fee-category', compact('data'));
   }

   public function add(){
   		return view('backend.setup.fee_category.add-fee-category');
   }
   public function store(Request $request){
   	$request->validate([
   		'name'		=> 'required|unique:feecategories,name',
   	]);
   	$data = new Feecategory();
   	$data->name = $request->name;
	$save = $data->save();
	   	if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Feecategory Inserted',
                'alert-type'    => 'success'
            );
	   	}return redirect()->route('setups.studentfeecategory.view')->with($notification);
   }

   public function edit($id){
   	$editData = Feecategory::find($id);
   	return view('backend.setup.fee_category.add-fee-category',compact('editData'));
   }

   public function update(Request $request,$id){
		$data = Feecategory::find($id);
		$request->validate([
   		'name'		=> 'required|unique:feecategories,name',
   		]);
   	$data->name = $request->name;
	$save = $data->save();
	   	if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Feecategory Updated',
                'alert-type'    => 'success'
            );
	   	}return redirect()->route('setups.studentfeecategory.view')->with($notification);
   }



}
