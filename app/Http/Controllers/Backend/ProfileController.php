<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use DB;
class ProfileController extends Controller
{
    public function view(){
    	$id = Auth::user()->id;

    	$user = User::find($id);
    	return view('backend.users.view-profile',compact('user'));
    }

    public function edit(){
    	$id = Auth::user()->id;
    	$editData = User::find($id);
    	return view('backend.users.edit-profile',compact('editData'));
    }

    public function update(Request $request){

        $data = User::find(Auth::user()->id);

        $data->name = $request->name;
        $data->title = $request->title;
        $data->address = $request->address;
        $data->mobile = $request->mobile;
        $data->gender = $request->gender;
        $data->about = $request->about;
        $data->q1 = $request->q1;
        $data->q2 = $request->q2;
        $data->q3 = $request->q3;
        $data->q4 = $request->q4;
        $data->q5 = $request->q5;
        $data->email = $request->email;
        $data->fb = $request->fb;
        $data->tw = $request->tw;
        $data->li = $request->li;

        if ($request->file('image')){
             $file = $request->file('image');
             @unlink(public_path('upload/user-images/'.$data->image));
             $filename = date('YmdHi').$file->getClientOriginalName();
             $file->move(public_path('upload/user-images'), $filename);
             $data->image = $filename;
         }

        $save = $data->save();

        if ($save) {
            $notification = array(
                'messagekey'        => 'Successfully Profile Updated ',
                'alert-type'    => 'success'
            );
            return redirect()->route('profiles.view')->with($notification);
        }
         
    }    



    public function passwordView(){
    	return view('backend.users.edit-password');
    }
    
    public function passwordUpdate(Request $request){
    	if (Auth::attempt(['id'=>Auth::user()->id, 'password'=>$request->current_password])) {
    		$user = User::find(Auth::user()->id);
    		$user->password = bcrypt($request->new_password);
    		$user->save();
    		$notification = array(
    			'messagekey'		=> 'Successfully changed password',
    			'alert-type'	=> 'success'
    		);
    		return redirect()->route('profiles.view')->with($notification);
    	}else{
    		$notification = array(
    			'messagekey'		=> 'Sorry!, your current password does not match',
    			'alert-type'	=> 'error'
    		);
    		return redirect()->back()->with($notification);
    	}
    }






}
