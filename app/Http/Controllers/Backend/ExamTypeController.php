<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Examtype;

class ExamTypeController extends Controller
{
    public function view(){
   	$data = Examtype::all();
   	return view('backend.setup.exam_type.view-exam-type', compact('data'));
   }

   public function add(){
   		return view('backend.setup.exam_type.add-exam-type');
   }
   public function store(Request $request){
   	$request->validate([
   		'name'		=> 'required|unique:examtypes,name',
   	]);
   	$data = new Examtype();
   	$data->name = $request->name;
	$save = $data->save();
	   	if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Exam Type Inserted',
                'alert-type'    => 'success'
            );
	   	}return redirect()->route('setups.examtype.view')->with($notification);
   }

   public function edit($id){
   	$editData = Examtype::find($id);
   	return view('backend.setup.exam_type.add-exam-type',compact('editData'));
   }

   public function update(Request $request,$id){
		$data = Examtype::find($id);
		$request->validate([
   		'name'		=> 'required|unique:examtypes,name',
   		]);
   	$data->name = $request->name;
	$save = $data->save();
	   	if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Exam Type Updated',
                'alert-type'    => 'success'
            );
	   	}return redirect()->route('setups.examtype.view')->with($notification);
   }    



}
