<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\Teacher;


class UsersController extends Controller
{
   public function view(){
   	$data = User::all();
   	return view('backend.users.view-users', compact('data'));
   }

   public function add(){
   		return view('backend.users.add-user');
   }

   public function store(Request $request){
   	$request->validate([
   		'usertype'		=> 'required',
   		'name'		=> 'required',
   		'email'		=> 'required|unique:users,email',
   		'password'		=> 'required',

   	]);

    $teacher_data = new Teacher();
   	$data = new User();
   	$data->usertype = $request->usertype;
   	$data->name = $request->name;
    $data->email = $request->email;
   	$teacher_data->email = $request->email;
   	$data->password = bcrypt($request->password);
   	$save = $data->save();
    $teacher_data->save();

	if ($save) {
			$notification = array(
        'messagekey'        => 'Successfully User Inserted ',
        'alert-type'    => 'success'
    );
	}
	   	return redirect()->route('users.view')->with($notification);
   }



   public function edit($id){
   	$editData = User::find($id);
   	return view('backend.users.edit-user', compact('editData'));
   }

   public function update(Request $request,$id){
		$data = User::find($id);
	   	$data->usertype = $request->usertype;
	   	$data->name = $request->name;
	   	$data->email = $request->email;
	   	$save = $data->save();
	   	if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully User Updated ',
                'alert-type'    => 'success'
            );
	   	}
	   	return redirect()->route('users.view')->with($notification);
   }
   
   public function delete($id){
   	$data = User::find($id);
   	$data->delete();
   	if(file_exists('public/upload/user-images/'.$data->image) AND ! empty($data->image)){
   		unlink('public/upload/user-images/'.$data->image);
   	}
	   $notification = array(
            'messagekey'        => 'Successfully User Deleted ',
            'alert-type'    => 'success'
        );

	   	return redirect()->route('users.view')->with($notification);
   }


   public function adminDashboard(){
    dd('admin.dashboard');
   }






}
