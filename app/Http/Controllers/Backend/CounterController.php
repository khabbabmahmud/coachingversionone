<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Model\Counter;

class CounterController extends Controller
{
    public function view(){
   	$data = Counter::all();
   	return view('backend.counter.view-counter', compact('data'));
   }
   public function edit($id){
   	$editData = Counter::find($id);
   	return view('backend.counter.edit-counter', compact('editData'));
   }
    public function update(Request $request,$id){
		$data = Counter::find($id);
		$data->one = $request->one;
		$data->one_title = $request->one_title;
		$data->two = $request->two;
		$data->two_title = $request->two_title;
		$data->three = $request->three;
		$data->three_title = $request->three_title;
		$data->four = $request->four;
		$data->four_title = $request->four_title;

		//return response()->json($data);

		$data->save();

		$notification = array(
        'messagekey'        => 'Successfully Counter Updated ',
        'alert-type'    => 'success'
   			 );

	   	return redirect()->route('counter.view')->with($notification);
		}
	   			
}

