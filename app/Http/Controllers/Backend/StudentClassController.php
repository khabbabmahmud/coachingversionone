<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\StudentClass;
use DB;


class StudentClassController extends Controller
{
    public function view(){
   	$data = StudentClass::all();
   	return view('backend.setup.studentclass.view-class', compact('data'));
   }

   public function add(){
   		return view('backend.setup.studentclass.add-class');
   }
   public function store(Request $request){
   	$request->validate([
   		'name'		=> 'required|unique:student_classes,name',
   	]);
   	$data = new StudentClass();
   	$data->name = $request->name;
	$save = $data->save();
	   	if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Class Inserted',
                'alert-type'    => 'success'
            );
	   	}return redirect()->route('setups.studentclass.view')->with($notification);
   }

   public function edit($id){
   	$editData = StudentClass::find($id);
   	return view('backend.setup.studentclass.add-class',compact('editData'));
   }

   public function update(Request $request,$id){
		$data = StudentClass::find($id);
		$request->validate([
   		'name'		=> 'required|unique:student_classes,name',
   		]);
   	$data->name = $request->name;
	$save = $data->save();
	   	if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Class Updated',
                'alert-type'    => 'success'
            );
	   	}return redirect()->route('setups.studentclass.view')->with($notification);
   }

}
