<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Footertext;

class footerController extends Controller
{
     public function view(){
    $data = Footertext::first();
   	return view('backend.footertext.view-footertext', compact('data'));
   }
    public function update(Request $request,$id){

		$data = Footertext::find($id);
		$data->text = $request->text; 
		$save = $data->save();
			if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Text Updated ',
                'alert-type'    => 'success'
            );
	   	}
	   	return redirect()->back()->with($notification);

		
	   			
   }
}
