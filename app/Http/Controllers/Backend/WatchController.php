<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Model\Watch;

class WatchController extends Controller
{
   public function view(){
   	$data = Watch::all();
   	return view('backend.watch.view-watch', compact('data'));
   }

   public function edit($id){
   	$editData = Watch::find($id);
   	return view('backend.watch.edit-watch', compact('editData'));
   }

	public function update(Request $request,$id){
		$data = Watch::find($id);
		$data->title = $request->title;
		$data->sub_title = $request->sub_title;
		$data->link = $request->link;
		$data->save();
		$notification = array(
		        'messagekey' => 'Successfully Watch Campus Updated',
		        'alert-type' => 'success'
		      ); return redirect()->route('watch.view')->with($notification);
	
	   			
   }

}
