<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\Feecategory;
use App\Model\StudentClass;
use App\Model\Feecategoryamount;

class FeeAmountController extends Controller
{
     public function view(){
   	$data = Feecategoryamount::select('fee_category_id')->groupBy('fee_category_id')->get();
   	return view('backend.setup.fee_amount.view-fee-amount', compact('data'));
   }

   public function add(){
   	$data['fee_categories'] = Feecategory::all();
   	$data['classes'] = StudentClass::all();
   		return view('backend.setup.fee_amount.add-fee-amount',$data);
   }
   public function store(Request $request){
  	$countClass = count($request->class_id);
  	if($countClass !=NULL){
  		for ($i=0; $i <$countClass ; $i++) { 
  			$fee_amount = new Feecategoryamount();
  			$fee_amount->fee_category_id = $request->fee_category_id;
  			$fee_amount->class_id = $request->class_id[$i];
  			$fee_amount->amount = $request->amount[$i];
  			$fee_amount->save();
  		}
  	}

  	$notification = array(
            'messagekey'        => 'Successfully Feecategory Inserted',
            'alert-type'    => 'success'
        );return redirect()->route('setups.studentfeeamount.view')->with($notification);
}

   public function edit($fee_category_id){
   	$data['editData'] = Feecategoryamount::where('fee_category_id', $fee_category_id)->orderBy('class_id','asc')->get();
   	$data['fee_categories'] = Feecategory::all();
   	$data['classes'] = StudentClass::all();
   	return view('backend.setup.fee_amount.edit-fee-amount',$data);
   }

   public function update(Request $request,$fee_category_id){
   		if ($request->class_id==NULL) {
   			$notification = array(
                'messagekey'        => 'Sorry! you do not select any item',
                'alert-type'    => 'error'
            );return redirect()->back()->with($notification);
   		}else{
   			Feecategoryamount::where('fee_category_id',$fee_category_id)->delete();

   			$countClass = count($request->class_id);
  			for ($i=0; $i <$countClass ; $i++) { 
  			$fee_amount = new Feecategoryamount();
  			$fee_amount->fee_category_id = $request->fee_category_id;
  			$fee_amount->class_id = $request->class_id[$i];
  			$fee_amount->amount = $request->amount[$i];
  			$fee_amount->save();
  		}
  		$notification = array(
            'messagekey'        => 'Successfully Fee Amount Updated',
            'alert-type'    => 'success'
        );return redirect()->route('setups.studentfeeamount.view')->with($notification);
   		}
   }
   public function details($fee_category_id){
   	$data['editData'] = Feecategoryamount::where('fee_category_id', $fee_category_id)->orderBy('class_id','asc')->get();
   	
   	return view('backend.setup.fee_amount.details-fee-amount',$data);
   }













}
