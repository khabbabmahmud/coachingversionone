<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Model\Slider;

class SliderController extends Controller
{
    public function view(){
   	$data = Slider::all();
   	return view('backend.slider.view-slider', compact('data'));
   }

    public function add(){
   		return view('backend.slider.add-slider');
   }

   public function store(Request $request){
   	// $data = new Slider();
    
   	$data->short_title = $request->short_title;
   	$data->long_title = $request->long_title;
   	
	if ($request->file('image')) {
		$file = $request->file('image');
		$filename = date('YmdHi').$file->getClientOriginalName();
		$file->move(public_path('upload/sliders'),$filename);
		$data->image= $filename;
	}
	$save = $data->save();
	   	if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Slider Updated ',
                'alert-type'    => 'success'
            );
	   	}
	   	return redirect()->route('sliders.view')->with($notification);

   }
   public function edit($id){
   	$editData = Slider::find($id);
   	return view('backend.slider.edit-slider',compact('editData'));
   }

   public function update(Request $request,$id){
		$data = Slider::find($id);
    
		$data->short_title = $request->short_title;
   	$data->long_title = $request->long_title;

		if ($request->file('image')) {
			$file = $request->file('image');
			@unlink(public_path('upload/sliders/'.$data->image));

			$filename = date('YmdHi').$file->getClientOriginalName();

			$file->move(public_path('upload/sliders'),$filename);
			$data->image= $filename;
		}
		$save = $data->save();
			if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Slider Updated ',
                'alert-type'    => 'success'
            );
	   	}
	   	return redirect()->route('sliders.view')->with($notification);
   }

   public function delete($id){
   	$data = Slider::find($id);
   	if (file_exists('public/upload/sliders/'.$data->image) AND ! empty($data->image)) {
   		unlink('public/upload/sliders/'.$data->image);
   	}
   	$data->delete();
		$notification = array(
    	'messagekey'        => 'Successfully Slider Deleted ',
    	'alert-type'    => 'info'
       );
	   	return redirect()->route('sliders.view')->with($notification);
   }





}
