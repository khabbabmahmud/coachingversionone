<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Faq;


class FaQController extends Controller
{
    public function view(){
   	$data = Faq::orderBy('id', 'desc')->get();
   	return view('backend.faq.view-faq', compact('data'));
   }

   public function add(){
   		return view('backend.faq.add-faq');
   }

   public function store(Request $request){
   		$data = new Faq();
   		$data->question = $request->question;
   		$data->ans = $request->ans;
	 	$data->save();
	   			$notification = array(
                'messagekey'        => 'Successfully FAQ Inserted',
                'alert-type'    => 'success'
                );
	   	return redirect()->route('faq.view')->with($notification);
   }

   public function edit($id){
   	$editData = Faq::find($id);
   	return view('backend.faq.edit-faq',compact('editData'));
   }

    public function update(Request $request,$id){
		$data = Faq::find($id);
		$data->question = $request->question;
	   	$data->ans 		= $request->ans;
		$data->save();
	   		$notification = array(
                'messagekey'        => 'Successfully FAQ Updated ',
                'alert-type'    => 'success'
            );return redirect()->route('faq.view')->with($notification);
   }


}
