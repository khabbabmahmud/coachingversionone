<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Communicate;
class Communicatecontroller extends Controller
{
    public function view(){
    $data = Communicate::orderBy('id','desc')->get();
   	return view('backend.communicate.view-communicate', compact('data'));
   }

   public function delete($id){
   	$data = Communicate::find($id);
   	$data->delete();
   	$notification = array(
            'messagekey'        => 'Successfully SMS Deleted ',
            'alert-type'    => 'success'
        );return redirect()->back()->with($notification);
   }
}
