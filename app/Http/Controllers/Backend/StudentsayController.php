<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Studentsay;

class StudentsayController extends Controller
{
    public function view(){
   	$data = Studentsay::orderBy('id', 'desc')->get();
   	return view('backend.studentsay.view-studentsay', compact('data'));
   }
   public function add(){
   		return view('backend.studentsay.add-studentsay');
   }
   public function store(Request $request){
   	// 	$request->validate([
   	// 	'name'		=> 'required',
   	// 	'title'		=> 'required',
   	// 	'message'	=> 'required',
   	// 	'image'		=> 'required',

   	// ]);
   	$data = new Studentsay();
   	$data->name = $request->name;
   	$data->title = $request->title;
   	$data->message = $request->message;
	if ($request->file('image')) {
		$file = $request->file('image');
		$filename = date('YmdHi').$file->getClientOriginalName();
		$file->move(public_path('upload/review'),$filename);
		$data->image= $filename;
	}
	$save = $data->save();
	   	if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Review Inserted',
                'alert-type'    => 'success'
            );
	   	}	return redirect()->route('studentsay.view')->with($notification);

   }
   public function edit($id){
   	$editData = Studentsay::find($id);
   	return view('backend.studentsay.edit-studentsay',compact('editData'));
   }

   public function update(Request $request,$id){
		$data = Studentsay::find($id);
		$data->name = $request->name;
	   	$data->title = $request->title;
	   	$data->message = $request->message;

		if ($request->file('image')) {
			$file = $request->file('image');
			@unlink(public_path('upload/review/'.$data->image));
			$filename = date('YmdHi').$file->getClientOriginalName();
			$file->move(public_path('upload/review'),$filename);
			$data->image= $filename;
		}
		$save = $data->save();
			if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Review Updated ',
                'alert-type'    => 'success'
            );
	   	}return redirect()->route('studentsay.view')->with($notification);
   }
   public function delete($id){
   	$data = Studentsay::find($id);
   	if (file_exists('public/upload/review/'.$data->image) AND ! empty($data->image)) {
   		unlink('public/upload/review/'.$data->image);
   	}
   	$data->delete();
		$notification = array(
    	'messagekey'        => 'Successfully Review Deleted',
    	'alert-type'    => 'info'
       );return redirect()->route('studentsay.view')->with($notification);
   }





}

