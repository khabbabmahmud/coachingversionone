<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Studentgroup;

class StudentGroupController extends Controller
{
    public function view(){
   	$data = Studentgroup::all();
   	return view('backend.setup.group.view-group', compact('data'));
   }

   public function add(){
   		return view('backend.setup.group.add-group');
   }
   public function store(Request $request){
   	$request->validate([
   		'name'		=> 'required|unique:studentgroups,name',
   	]);
   	$data = new Studentgroup();
   	$data->name = $request->name;
	$save = $data->save();
	   	if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Group Inserted',
                'alert-type'    => 'success'
            );
	   	}return redirect()->route('setups.studentgroup.view')->with($notification);
   }

   public function edit($id){
   	$editData = Studentgroup::find($id);
   	return view('backend.setup.group.add-group',compact('editData'));
   }

   public function update(Request $request,$id){
		$data = Studentgroup::find($id);
		$request->validate([
   		'name'		=> 'required|unique:studentgroups,name',
   		]);
   		$data->name = $request->name;
		$save = $data->save();
	   	if ($save) {
	   			$notification = array(
                'messagekey'        => 'Successfully Group Updated',
                'alert-type'    => 'success'
            );
	   	}return redirect()->route('setups.studentgroup.view')->with($notification);
   }

}
