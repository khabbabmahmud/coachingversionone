<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Logo;
use App\Model\Slider;
use App\Model\Contactinfo;
use App\Model\Welcometo;
use App\Model\Course;
use App\Model\Watch;
use App\Model\Counter;
use App\Model\Communicate;
use App\Model\Faq;
use App\Model\Studentsay;
use App\Model\Partner;
use App\Model\Footertext;
use DB;
use App\User;
use Mail;


class frontend extends Controller
{
   public function index(){
      $text = Footertext::first();
      $partners = Partner::all();
      $studentsays = Studentsay::all();
      $logo1 = Logo::where('id', 1)->first();
      $logo2 = Logo::where('id', 2)->first();
      $sliders = Slider::all();
      $welcometo = Welcometo::where('id', 1)->first();
      $courses = Course::all();
      $contactinfo = Contactinfo::first();
      $lacturars = User::where('usertype', 'Teacher')->get();
      $watch = Watch::first();
      $counter = Counter::first();
      return view('index', compact('logo1','logo2','sliders','welcometo','courses','contactinfo','lacturars','watch','counter','studentsays','partners','text'));
   }


   public function lecturers(){
      $text = Footertext::first();
      $lacturars = User::where('usertype', 'Teacher')->get();
      $contactinfo = Contactinfo::first();
      $logo1 = Logo::where('id', 1)->first();
      $logo2 = Logo::where('id', 2)->first();

   	return view('lecturers', compact('logo1','logo2','contactinfo','lacturars','text'));

   }

   public function about(){
      $text = Footertext::first();
      $lacturars = User::where('usertype', 'Teacher')->get();
      $counter = Counter::first();
      $sliders = Slider::all();
      $logo1 = Logo::where('id', 1)->first();
      $logo2 = Logo::where('id', 2)->first();
      $contactinfo = Contactinfo::first();
   	return view('about',compact('logo1','logo2','contactinfo','sliders','counter','lacturars','text'));
   }

   public function singleLecturer($id){
      $data = User::find($id);
      $text = Footertext::first();
      $logo1 = Logo::where('id', 1)->first();
      $logo2 = Logo::where('id', 2)->first();
      $contactinfo = Contactinfo::first();
      return view('single-lecturers',compact('logo1','logo2','contactinfo','data','text'));
 
   }

   public function registration(){
      $text = Footertext::first();
      $logo1 = Logo::where('id', 1)->first();
      $logo2 = Logo::where('id', 2)->first();
      $contactinfo = Contactinfo::first();
      return view('registration', compact('logo1','logo2','contactinfo','text'));
   }

   public function faq(){
      $text = Footertext::first();
      $faqs = Faq::orderBy('id', 'desc')->get();
      $logo1 = Logo::where('id', 1)->first();
      $logo2 = Logo::where('id', 2)->first();
      $contactinfo = Contactinfo::first();
   	return view('faq', compact('logo1','logo2','contactinfo','faqs','text'));
   }
   public function error_page(){
   	return view('404');
   }

   public function news(){
      $text = Footertext::first();
      $contactinfo = Contactinfo::first();
      $logo1 = Logo::where('id', 1)->first();
      $logo2 = Logo::where('id', 2)->first();
      return view('news', compact('logo1','logo2','contactinfo','text'));
   }

   public function single_news(){
      $text = Footertext::first();
      $contactinfo = Contactinfo::first();
      $logo1 = Logo::where('id', 1)->first();
      $logo2 = Logo::where('id', 2)->first();
      return view('single-news',compact('logo1','logo2','info','contactinfo','text'));
   }

	public function event(){
      $text = Footertext::first();
      $logo1 = Logo::where('id', 1)->first();
      $logo2 = Logo::where('id', 2)->first();
      $contactinfo = Contactinfo::first();
      return view('event',compact('logo1','logo2','contactinfo','text'));
	  
	   }

	public function single_event(){
      $text = Footertext::first();
	  $contactinfo = Contactinfo::first();
      $logo1 = Logo::where('id', 1)->first();
      $logo2 = Logo::where('id', 2)->first();
      return view('single-event',compact('logo1','logo2','contactinfo','text'));
	   }

   public function gallery(){
      $text = Footertext::first();
      $logo1 = Logo::where('id', 1)->first();
      $logo2 = Logo::where('id', 2)->first();
      $contactinfo = Contactinfo::first();
      return view('gallery',compact('logo1','logo2','contactinfo','text'));
   }

   public function contact(){
      $text = Footertext::first();
      $logo1 = Logo::where('id', 1)->first();
      $logo2 = Logo::where('id', 2)->first();
      $contactinfo = Contactinfo::first();
      return view('contact',compact('logo1','logo2','contactinfo','text'));
   }

   public function communicateStore(Request $request){
      $communicate = new Communicate();
      $communicate->name = $request->name;
      $communicate->email = $request->email;
      $communicate->phone = $request->phone;
      $communicate->address = $request->address;
      $communicate->message = $request->msg;
      $save = $communicate->save();

      $data = array(
         'name'      => $request->name,
         'email'     => $request->email,
         'phone'     => $request->phone,
         'address'   => $request->address,
         'msg'   => $request->msg,
      );
      Mail::send('emails.communicate', $data, function($message) use($data){
         $message->from('educationalhomepsdc@gmail.com','Practicing Science Dot Com');
         $message->to($data['email']);
         $message->subject('Thanks for contact us');

      });


      if ($save) {
         $notification = array(
            'messagekey'        => 'We Received Your Message, Thank You.',
            'alert-type'    => 'success',
        ); return redirect()->back()->with($notification);
      }else{
         $notification = array(
            'messagekey'        => 'Something went wrong ',
            'alert-type'    => 'error',
        ); return redirect()->back()->with($notification);
      }


      

   }

   public function studentLogin(){
      $text = Footertext::first();
      $logo1 = Logo::where('id', 1)->first();
      $logo2 = Logo::where('id', 2)->first();
      $contactinfo = Contactinfo::first();
   	return view('login', compact('logo1','logo2','contactinfo','text'));
   }


   












}
