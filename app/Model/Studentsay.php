<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Studentsay extends Model
{
    protected $fillable = [
   		'name','title','message','image','info',
    ];
}
