<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Contactinfo extends Model
{
    protected $fillable = [
        'number','email','address','facebook','twitter','linkedin','pinterest','rss','googleplus',
    ];
}
