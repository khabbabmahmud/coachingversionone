<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Footertext extends Model
{
    protected $fillable = [
    'text', 'created_by', 'updated_by',
    ];
}
