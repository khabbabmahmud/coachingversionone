<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Examtype extends Model
{
    protected $fillable = [
        'name',
    ];
}
