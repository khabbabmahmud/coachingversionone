<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Welcometo extends Model
{
    protected $fillable = [
        'short_title', 'long_title', 'sone_title', 'sone_subtitle', 'stwo_title','stwo_subtitle', 'sthree_title', 'sthree_subtitle', 'created_by', 'updated_by',
    ];
}
