<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Feecategoryamount extends Model
{
    public function fee_category(){
    	return $this->belongsTo(Feecategory::class, 'fee_category_id','id');
    }
    public function student_class(){
    	return $this->belongsTo(StudentClass::class, 'class_id','id');
    }
}
