<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
	 protected $fillable = [
    'image', 'link', 'short_title','long_title', 'duration', 'classess','class_time', 'created_by', 'updated_by',
    ];
}
