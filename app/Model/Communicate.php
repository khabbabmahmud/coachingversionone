<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Communicate extends Model
{
    protected $fillable = [
        'name','email','phone','address','message',
    ];
}

