<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $fillable = [
    'question', 'ans', 'created_by', 'updated_by',
    ];
}
