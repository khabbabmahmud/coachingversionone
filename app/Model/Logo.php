<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Logo extends Model
{
    protected $fillable = [
        'image', 'info', 'created_by', 'updated_by',
    ];
}
