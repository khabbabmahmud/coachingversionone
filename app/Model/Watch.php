<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Watch extends Model
{
    protected $fillable = [
        'title', 'sub_title', 'link',
    ];
}
