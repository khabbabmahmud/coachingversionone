<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Counter extends Model
{
    protected $fillable = [
        'one', 'one_title', 'two','two_title', 'three', 'three_title','four', 'four_title', 'image',
    ];
}
